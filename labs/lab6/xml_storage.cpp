#include "xml_storage.h"
 #include <QString>
 #include <QFile>
 #include <QDebug>
#include <fstream>
#include <QtXml>
using namespace std;
metroStation domElementToMetroStation(QDomElement & element)
{
    metroStation c;
    c.id=element.attribute("id").toInt();
    c.nameOfStation=element.attribute("nameOfStation").toStdString();
    c.hourOfOpening=element.attribute("hourOfOpening").toInt();
    c.nameOfLine=element.attribute("nameOfLine").toStdString();
     return c;
}
QDomElement metroStationToDomElement(QDomDocument & doc, metroStation & c)
{
    QDomElement metroStation_el = doc.createElement("metroStation");
    metroStation_el.setAttribute("id", c.id);
    metroStation_el.setAttribute("nameOfStation", c.nameOfStation.c_str());
    metroStation_el.setAttribute("hourOfOpening", c.hourOfOpening);
    metroStation_el.setAttribute("nameOfLine", c.nameOfLine.c_str());
    return metroStation_el;
}
metroLine domElementToMetroLine(QDomElement & element)
{
    metroLine c ;//language c
    c.id=element.attribute("id").toInt();
    c.nameOfline=element.attribute("nameOfLine").toStdString();
    c.amountOfstation=element.attribute("amountOfStations").toInt();
    c.colourOfLine=element.attribute("colourOfLine").toStdString();
     return c;
}
QDomElement metroLineToDomElement(QDomDocument & doc, metroLine & c)
{
    QDomElement metroLine_el = doc.createElement("metroLine");
    metroLine_el.setAttribute("id", c.id);
    metroLine_el.setAttribute("nameOfLine", c.nameOfline.c_str());
    metroLine_el.setAttribute("amountOfStations", c.amountOfstation);
    metroLine_el.setAttribute("colourOfLine", c.colourOfLine.c_str());
    return metroLine_el;
}


bool XmlStorage::load()
{
    {
        string filename =this->dir_name_+ "/metroStation.xml";
          QString q_filename=QString::fromStdString(filename);
          QFile file(q_filename);
          bool is_opened=file.open(QFile::ReadOnly);

      if(!is_opened)
        {
            qDebug()<< "file not opened" <<q_filename;
            return false;
        }
        QTextStream ts(&file);
        QString text=ts.readAll();
          file.close();
        QDomDocument doc;
        QString errorMessage;
        int errorLine;
        int errorColumn;
        if(!doc.setContent(text, &errorMessage, &errorLine , &errorColumn))
        {
              qDebug()<<"Error parsing XML text"<< errorMessage;
              qDebug()<<"at line"<< errorLine<<"column "<< errorColumn;
              return false;
        }
        QDomElement root =doc.documentElement();
        for(int i=0;i< root.childNodes().size();i++)
        {
          QDomNode node = root.childNodes().at(i);
          QDomElement element = node.toElement();
          metroStation c=domElementToMetroStation(element);
          this->metroStations_.push_back(c);
      }
        file.close();

    }

//metroLines

  string filename =this->dir_name_+ "/metroLines.xml";
    QString q_filename=QString::fromStdString(filename);
    QFile file(q_filename);
    bool is_opened=file.open(QFile::ReadOnly);

if(!is_opened)
  {
      qDebug()<< "file not opened" <<q_filename;
      return false;
  }
  QTextStream ts(&file);
  QString text=ts.readAll();
    file.close();
  QDomDocument doc;
  QString errorMessage;
  int errorLine;
  int errorColumn;
  if(!doc.setContent(text, &errorMessage, &errorLine , &errorColumn))
  {
        qDebug()<<"Error parsing XML text"<< errorMessage;
        qDebug()<<"at line"<< errorLine<<"column "<< errorColumn;
        return false;
  }
  QDomElement root =doc.documentElement();
  for(int i=0;i< root.childNodes().size();i++)
  {
    QDomNode node = root.childNodes().at(i);
    QDomElement element = node.toElement();
    metroLine c=domElementToMetroLine(element);
    this->metroLines_.push_back(c);
  }
  file.close();
  return true;
}

// interesting записування тексту у файл
bool XmlStorage::save()
{

{
  QDomDocument doc;
  QDomElement root=doc.createElement("metroStations");
  for(metroStation & c: this->metroStations_)
  {
        QDomElement metroStation_el=metroStationToDomElement(doc,c);
       root.appendChild(metroStation_el);
  }
  doc.appendChild(root);
  QString xml_text =doc.toString(4);
  string filename =this->dir_name_+ "/metroStation.xml";
    QString q_filename=QString::fromStdString(filename);
    QFile file(q_filename);
    bool is_opened=file.open(QFile::WriteOnly);

if(!is_opened)
  {
      qDebug()<< "file not opened for writing" <<q_filename;
      return false;
  }
QTextStream ts(&file);
ts << xml_text;
file.close();
}


    QDomDocument doc;
    QDomElement root=doc.createElement("metroLines");
    for(metroLine & c: this->metroLines_)
    {
         QDomElement metroLine_el=metroLineToDomElement(doc,c);
         root.appendChild(metroLine_el);
    }
    doc.appendChild(root);
    QString xml_text =doc.toString(4);
    string filename =this->dir_name_+ "/metroLines.xml";
      QString q_filename=QString::fromStdString(filename);
      QFile file(q_filename);
      bool is_opened=file.open(QFile::WriteOnly);

  if(!is_opened)
    {
        qDebug()<< "file not opened for writing" <<q_filename;
        return false;
    }
  QTextStream ts(&file);
  ts << xml_text;
  file.close();
    return true;
}

////////////metroStation
vector<metroStation> XmlStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations = this->metroStations_;
    return metroStations;
}
bool XmlStorage::updatemetroStation(const metroStation &metroStation, char ch)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation.id == this->metroStations_[i].id)
        {
            if (ch == '1')
                this->metroStations_[i].nameOfStation = metroStation.nameOfStation;
            else if (ch == '2')
                this->metroStations_[i].nameOfLine = metroStation.nameOfLine;
            else if (ch == '3')
                this->metroStations_[i].hourOfOpening = metroStation.hourOfOpening;
            this->save();
            return true;
        }
    }
    return false;
}
bool XmlStorage::removemetroStation(int metroStation_id)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation_id == this->metroStations_[i].id)
        {
            this->metroStations_.erase(this->metroStations_.begin() + i);
            this->save();
            return true;
        }
    }
    return false;
}

int XmlStorage::insertmetroStation(const metroStation &metroStation)
{
    struct metroStation copy;
    copy.hourOfOpening = metroStation.hourOfOpening;
    copy.nameOfLine = metroStation.nameOfLine;
    copy.nameOfStation = metroStation.nameOfStation;
    int id = getNewMetroStationId();
    copy.id = id;
    this->metroStations_.push_back(copy);
    this->save();
    return id;
}
int XmlStorage::getNewMetroStationId()
{
    ifstream fp;
    string filename = this->dir_name_ + "/id.txt";
    fp.open(filename);
    if (!fp.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    string idStr;
    getline(fp, idStr);
    int id = stoi(idStr);
    fp.close();
    ofstream fout;
    fout.open(filename);
    if (!fout.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    id++;
    idStr = to_string(id);
    fout << idStr;
    return id;
}

optional<metroStation> XmlStorage::getmetroStationById(int metroStation_id)
{
    optional<metroStation> optStation;
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (this->metroStations_[i].id == metroStation_id)
        {
            optStation = metroStations_[i];
            return optStation;
        }
    }
    return optStation;
}

////metroLine

vector<metroLine> XmlStorage::getAllmetroLines(void)
{
    vector<metroLine> metroLines = this->metroLines_;
    return metroLines;
}
optional<metroLine> XmlStorage::getmetroLineById(int metroLine_id)
{
    optional<metroLine> optStation;
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (this->metroLines_[i].id == metroLine_id)
        {
            optStation = metroLines_[i];
            return optStation;
        }
    }
    return optStation;
}
bool XmlStorage::updatemetroLine(const metroLine &metroLine, char ch)
{
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (metroLine.id == this->metroLines_[i].id)
        {
            if (ch == '1')
            {
                puts("hi");
                this->metroLines_[i].nameOfline = metroLine.nameOfline;
            }
            else if (ch == '2')
            {
                puts("hi");
                this->metroLines_[i].amountOfstation = metroLine.amountOfstation;
            }

            else if (ch == '3')
            {
                puts("hi");
                this->metroLines_[i].colourOfLine = metroLine.colourOfLine;
            }

            this->save();
            return true;
        }
    }
    return false;
}
bool XmlStorage::removemetroLine(int metroLine_id)
{
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (metroLine_id == this->metroLines_[i].id)
        {
            this->metroLines_.erase(this->metroLines_.begin() + i);
            this->save();
            return true;
        }
    }
    return false;
}
int XmlStorage::insertmetroLine(const metroLine &metroLine)
{
    struct metroLine copy;
    copy.nameOfline = metroLine.nameOfline;
    copy.amountOfstation = metroLine.amountOfstation;
    copy.colourOfLine = metroLine.colourOfLine;
    int id = getNewMetroLineId();
    copy.id = id;
    this->metroLines_.push_back(copy);
    this->save();
    return id;
}
int XmlStorage::getNewMetroLineId()
{
    ifstream fp;
    string filename = this->dir_name_ + "/lineId.txt";
    fp.open(filename);
    if (!fp.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    string idStr;
    getline(fp, idStr);
    int id = stoi(idStr);
    fp.close();
    ofstream fout;
    fout.open(filename);
    if (!fout.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    id++;
    idStr = to_string(id);
    fout << idStr;
    return id;
}
