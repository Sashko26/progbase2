#pragma once
#include <stdlib.h>   // Деякі додаткові функції

typedef struct Deque
{
    float *array;
    int length;
    int capacity;
    int front;

} Deque;
void Deque_init(Deque *self);
void Deque_deinit(Deque *self);
void Deque_pushByFront(Deque *self, float value);
float Deque_popByFront(Deque *self);
void Deque_pushByRear(Deque *self, float value);
float Deque_popByRear(Deque *self);
float frontPeek(Deque *self);
float rearPeek(Deque *self);

int Deque_size(Deque *self);
void Deque_print(Deque *self);
