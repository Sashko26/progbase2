#include "xml_storage.h"
#include <QString>
#include <QFile>
#include <QDebug>
#include <fstream>
#include <QtXml>
#include <QDir>
using namespace std;
metroStation domElementToMetroStation(QDomElement & element)
{
    metroStation c;
    c.id=element.attribute("id").toInt();
    c.nameOfStation=element.attribute("nameOfStation").toStdString();
    c.hourOfOpening=element.attribute("hourOfOpening").toInt();
    c.nameOfLine=element.attribute("nameOfLine").toStdString();
    return c;
}
QDomElement metroStationToDomElement(QDomDocument & doc, metroStation & c)
{
    QDomElement metroStation_el = doc.createElement("metroStation");
    metroStation_el.setAttribute("id", c.id);
    metroStation_el.setAttribute("nameOfStation", c.nameOfStation.c_str());
    metroStation_el.setAttribute("hourOfOpening", c.hourOfOpening);
    metroStation_el.setAttribute("nameOfLine", c.nameOfLine.c_str());
    return metroStation_el;
}
bool XmlStorage::load()
{
    {

        QString Qdir_storage=QString::fromStdString(this->dir_name_);
        QDir storageDir(Qdir_storage);
        if(storageDir.exists()==false)//перевірка на існування директорії
        {
           storageDir.mkpath(Qdir_storage);// достворення всіх батьківських директорій, і самої директорії, якщо її не існує.
        }
        string filename =this->dir_name_+ "/metroStation.xml";
        QString q_filename=QString::fromStdString(filename);
        QFile file(q_filename);
        if(!file.exists())//перевірка на існування файлу в заданій директорії
        {
           (file.open(QIODevice::WriteOnly | QIODevice::Append));// якщо файлу не існує, то ми його створимо
            file.close();
        }
        bool is_opened=file.open(QFile::ReadOnly);
        if(!is_opened)
        {
            qDebug()<< "file not opened" <<q_filename;
            return false;
        }
        string filenameId=this->dir_name_+ "/id.txt";
        QString q_filenameId=QString::fromStdString(filenameId);
        QFile fileId(q_filenameId);
        if(!fileId.exists())
        {
            if (fileId.open(QIODevice::WriteOnly | QIODevice::Append))
            {
                fileId.write("1");
                fileId.close();
            }

        }
        fileId.close();
        QTextStream ts(&file);
        QString text=ts.readAll();
        file.close();
        QDomDocument doc;
        QString errorMessage;
        int errorLine;
        int errorColumn;
        if(!doc.setContent(text, &errorMessage, &errorLine , &errorColumn))
        {
            qDebug()<<"Error parsing XML text"<< errorMessage;
            qDebug()<<"at line"<< errorLine<<"column "<< errorColumn;
            return false;
        }
        QDomElement root =doc.documentElement();
        for(int i=0;i< root.childNodes().size();i++)
        {
            QDomNode node = root.childNodes().at(i);
            QDomElement element = node.toElement();
            metroStation c=domElementToMetroStation(element);
            this->metroStations_.push_back(c);
        }
        file.close();

    }
    return true;
}

// interesting записування тексту у файл
bool XmlStorage::save()
{
    QDomDocument doc;
    QDomElement root=doc.createElement("metroStations");
    for(metroStation & c: this->metroStations_)
    {
        QDomElement metroStation_el=metroStationToDomElement(doc,c);
        root.appendChild(metroStation_el);
    }
    doc.appendChild(root);
    QString xml_text =doc.toString(4);
    string filename =this->dir_name_+ "/metroStation.xml";
    QString q_filename=QString::fromStdString(filename);
    QFile file(q_filename);
    bool is_opened=file.open(QFile::WriteOnly);

    if(!is_opened)
    {
        qDebug()<< "file not opened for writing" <<q_filename;
        return false;
    }
    QTextStream ts(&file);
    ts << xml_text;
    file.close();


    return true;
}

////////////metroStation
vector<metroStation> XmlStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations = this->metroStations_;
    return metroStations;
}
bool XmlStorage::updatemetroStation(const metroStation &metroStation)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation.id == this->metroStations_[i].id)
        {

                this->metroStations_[i].nameOfStation = metroStation.nameOfStation;

                this->metroStations_[i].nameOfLine = metroStation.nameOfLine;

                this->metroStations_[i].hourOfOpening = metroStation.hourOfOpening;
            this->save();
            return true;
        }
    }
    return false;
}
bool XmlStorage::removemetroStation(int metroStation_id)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation_id == this->metroStations_[i].id)
        {
            this->metroStations_.erase(this->metroStations_.begin() + i);
            this->save();
            return true;
        }
    }
    return false;
}

int XmlStorage::insertmetroStation(const metroStation &metroStation)
{
    struct metroStation copy;
    copy.hourOfOpening = metroStation.hourOfOpening;
    copy.nameOfLine = metroStation.nameOfLine;
    copy.nameOfStation = metroStation.nameOfStation;
    int id = getNewMetroStationId();
    copy.id = id;
    this->metroStations_.push_back(copy);
    this->save();
    return id;
}
int XmlStorage::getNewMetroStationId()
{
    ifstream fp;
    string filename = this->dir_name_ + "/id.txt";
    fp.open(filename);
    if (!fp.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    string idStr;
    getline(fp, idStr);
    int id = stoi(idStr);
    fp.close();
    ofstream fout;
    fout.open(filename);
    if (!fout.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    id++;
    idStr = to_string(id);
    fout << idStr;
    return id;
}

optional<metroStation> XmlStorage::getmetroStationById(int metroStation_id)
{
    optional<metroStation> optStation;
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (this->metroStations_[i].id == metroStation_id)
        {
            optStation = metroStations_[i];
            return optStation;
        }
    }
    return optStation;
}


