#pragma once
#include <iostream>
#include <string>
#include <QMetaType>
using namespace std;
struct metroLine
{
    int id;
    string nameOfline;
    int amountOfstation;
    string colourOfLine;
};
Q_DECLARE_METATYPE(metroLine)
