#include "storage.h"
#include "cui.h"

int main(int argc, char const *argv[])
{
    Storage storage("./data");
    storage.load();
    Cui cui(&storage);
    cui.show();

    return 0;
}
