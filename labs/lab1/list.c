#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "list.h"

// перенесення членів списку з нульвим дробом на початок
void DynArray_zeroFractionAtBegin(DynArray *self)
{
    for (int i = 1; i < self->length; i++)
    {
        int j = i;
        while (j != 0)
        {
            if (fabs(self->array[j] - (int)self->array[j]) < 0.000001 && fabs(self->array[j - 1] - (int)self->array[j - 1]) > 0.000001)
            {
                int buf = self->array[j];
                self->array[j] = self->array[j - 1];
                self->array[j - 1] = buf;
            }
            j--;
        }
    }
}
// ініціалізація динамічного масиву
void DynArray_init(DynArray *self)
{
    self->length = 0;
    self->capacity = 4;
    self->array = malloc(sizeof(int) * self->capacity);
    if (self->array == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
}
// видалення диначічного масиву
void DynArray_deinit(DynArray *self)
{
    free(self->array);
}
// перевиділення пам'яті для динамічного масиву
static void DynArray_realloc(DynArray *self, int newCapacity)
{
    float *newArray = realloc(self->array, sizeof(float) * newCapacity);
    if (newArray == NULL)
    {
        free(self->array);
        fprintf(stderr, "Reallocation error");
        abort();
    }
    self->array = newArray;
    self->capacity = newCapacity;
}

void DynArray_add(DynArray *self, float value)
{
    self->array[self->length] = value;
    //
    self->length += 1;
    //
    if (self->length == self->capacity)
    {
        // realloc
        int newCapacity = self->capacity * 2;
        DynArray_realloc(self, newCapacity);
    }
}

void DynArray_print(const DynArray *self)
{
    for (int i = 0; i < self->length; i++)
    {
        float value = self->array[i];
        printf("%f, ", value);
    }
    printf("\n");
}
void DynArray_fromStringToArray(DynArray *self, char *string, int N)
{
    char helpStr[N];
    for (int i = 0; i < strlen(string);)
    {
        if (isdigit(string[i]))
        {
            int k = 0;
            while (isdigit(string[i]) || string[i] == '.')
            {
                helpStr[k] = string[i];
                k++;
                i++;
            }
            helpStr[k] = '\0';
            DynArray_add(self, atof(helpStr));
        }
        else
        {
            i++;
        }
    }
}
// отримання значення за індексом
float DynArray_getValueByIndex(DynArray *self,int index)
{
    float val= self->array[index];
    return val;
}

// отримання значення розміру списку
int  DynArray_size(DynArray *self)
{
    int val=self->length;
    return val;
}