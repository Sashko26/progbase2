#include "csv.h"
static string Csv_fillStringFromCsvRow(CsvRow &row)
{
    string str;
    for (int i = 0; i < row.size() - 1; i++)
    {
        str = str + row[i] + ",";
    }
    str = str+row[row.size() - 1] + "\n";
    return str;
}
string Csv::createStringFromTable(CsvTable &csv_table)
{
    string str;
    for (int i = 0; i < csv_table.size(); i++)
    {
        CsvRow ref = csv_table[i];
        str = str + Csv_fillStringFromCsvRow(ref);
    }
    return str;
}


CsvTable Csv::createTableFromString(string &csv_str)
{
    CsvTable table;
    CsvRow row;
    string bufStr;
    string::iterator p = csv_str.begin();
    while (1)
    {
        if (*p == ',')
        {
            row.push_back(bufStr);
            bufStr.clear();
        }
        else if (*p == '\n')
        { 
            row.push_back(bufStr);
            bufStr.clear();
            table.push_back(row);  
            row.clear();
        }
        else if (*p == '\0')
        {
            row.push_back(bufStr);
            bufStr.clear();
            table.push_back(row);
            row.clear();
        }
        else
        {
            bufStr = bufStr + (*p);
        }

        if (*p == '\0')
        {
            break;
        }
        p++;
    }
    return table;
}

