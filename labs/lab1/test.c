// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>    // Для друку в термінал
#include <math.h>     // Для математичних функцій
#include <stdlib.h>   // Деякі додаткові функції
#include <progbase.h> // Спрощений ввід і вивід даних у консоль
#include <string.h> 

int main() {
    // Початок програми
    char string[10]="asf";
    printf("|%ld|",strlen(string));
    string[3]='\0';
    string[4]='\0';
    string[5]='\0';
    printf("|%ld|",strlen(string));
    string[4]='c';
    printf("|%ld|",strlen(string));
    // Кінець програми
    return 0;
}