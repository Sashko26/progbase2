#pragma once

#include "optional.h"
#include "metroStation.h"
#include "storage.h"

using std::string;
using std::vector;
using namespace std;
class XmlStorage : public Storage
{
    const string dir_name_;
    vector<metroStation> metroStations_;
    int getNewMetroStationId();

  public:
    XmlStorage(const string &dir_name) : dir_name_(dir_name) {}
    bool open();
    bool close();
    // metroStations
    vector<metroStation> getAllmetroStations(void);
    optional<metroStation> getmetroStationById(int metroStation_id);
    bool updatemetroStation(const metroStation &metroStation);
    bool removemetroStation(int metroStation_id);
    int insertmetroStation(const metroStation &metroStation);
};
