#include "deque.hpp"

// функція для отримання значення передньої вершини
float Deque::frontPeek()
{
    float val = this->_array[this->_front];
    return val;
}
// функція для отримання значення задньої вершини
float Deque::rearPeek()
{
    float val = this->_array[this->_front + this->_length - 1];
    return val;
}
int Deque::size()
{
    int val = this->_length;
    return val;
}
void Deque::pushByFront(float value)
{
    if (this->_front == 0)
    {
        int newCapacity = this->_capacity * 2;
        Deque::enlarge(newCapacity);
        for (int i = 0; i < this->_length; i++)
        {
            this->_array[this->_front + i + this->_length] = this->_array[this->_front + i];
        }
        this->_array[this->_front + this->_length - 1] = value;
        this->_front = this->_front + this->_length - 1;
        this->_length = this->_length + 1;
    }
    else if (this->_front == -1)
    {
        this->_front = 0;
        this->_array[this->_front] = value;
        this->_length = this->_length + 1;
    }
    else
    {
        this->_front = this->_front - 1;
        this->_array[this->_front] = value;
        this->_length = this->_length + 1;
    }
}
int Deque::indexOfFrontPeek()
{
    int val = this->_front;
    return val;
}
int Deque::indexOfRearPeek()
{
    int val = this->_front + this->_length - 1;
    return val;
}

//видалення елементу деки з початку
float Deque::popByFront()
{
    if (this->_length == 0)
    {
        fprintf(stderr, "stack is underflow");
        abort();
    }
    else if (this->_length == 1)
    {
        int val = this->_array[this->_front];
        this->_length = this->_length - 1;
        this->_front = -1;
        return val;
    }
    else
    {
        this->_front = this->_front + 1;
        this->_length = this->_length - 1;
        return this->_array[this->_front - 1];
    }
}
float Deque::getValueByindex(int index)
{
    float val = this->_array[this->_front + index];
    return val;
}
//видалення елементу деки з кінця
float Deque::popByRear()
{
    if (this->_length == 0)
    {
        cout << "stack is underflow" << endl;
        abort();
    }
    float val = this->_array[this->_front + this->_length - 1];
    this->_length = this->_length - 1;
    return val;
}
// вставлення елементу деки в кінець
void Deque::pushByRear(float value)
{
    if (this->_front + this->_length == this->_capacity)
    {
        int newCapacity = this->_capacity * 2;
        enlarge(newCapacity);
        this->_length = this->_length + 1;
        this->_array[this->_front + this->_length - 1] = value;
        this->_capacity = newCapacity;
    }
    else if (this->_front == -1)
    {
        this->_front = 0;
        this->_length = 1;
        this->_array[this->_front] = value;
    }
    else
    {
        this->_length = this->_length + 1;
        this->_array[this->_front + this->_length - 1] = value;
    }
}
void Deque::print()
{
    if (this->_front == -1)
    {
        cout << "Deque is empty!" << endl;
    }
    else
    {
        int beginOfprint = this->_front;
        int endOfprint = this->_front + this->_length;
        for (int i = beginOfprint; i < endOfprint; i++)
        {
            cout << this->_array[i] << ", ";
        }
        cout << endl;
    }
}

Deque::Deque()
{
    this->_length = 0;
    this->_front = -1;
    this->_capacity = 4;
    this->_array = static_cast<float*>(malloc(sizeof(float)*this->_capacity));
    if (this->_array == nullptr)
    {
        cout << "Allocation error!" << endl;
        abort();
    }
}
Deque::~Deque()
{
    free(this->_array);
}