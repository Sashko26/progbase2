#ifndef AUTHENTICATION_H
#define AUTHENTICATION_H

#include <QDialog>
#include <optional.h>
#include <user.h>
#include <QString>


namespace Ui {
class authentication;
}

class authentication : public QDialog
{
    Q_OBJECT

public:
    explicit authentication(QWidget *parent = 0);
    ~authentication();
    QString getPassword();
    QString getLogin();
    void LabelInCorrect();


    private:
    Ui::authentication *ui;
};

#endif // AUTHENTICATION_H
