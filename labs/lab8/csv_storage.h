#pragma once

#include <vector>
#include <string>
#include "optional.h"
#include "metroStation.h"
#include "metroLine.h"
#include "csv.h"
#include <fstream>
#include <iostream>
#include <progbase/console.h>
#include <progbase.h>
#include <cstdio>
#include <string.h>
#include "storage.h"

using std::string;
using std::vector;
using namespace std;
class CsvStorage : public Storage
{
    const string dir_name_;
    vector<metroStation> metroStations_;
    vector<metroLine> metroLines_;
    static metroStation rowToMetroStation(const CsvRow &row)
    {
        metroStation instance;
        
        instance.id = stof(row[0]);
        cout<<instance.id<<endl;
        instance.nameOfStation = row[1];
        instance.hourOfOpening = stoi(row[2]);
        instance.nameOfLine = row[3];
        return instance;
    }
    static CsvRow metroStationToRow(const metroStation &st)
    {
        CsvRow row;
        row.push_back(to_string(st.id));
        row.push_back(st.nameOfStation);
        row.push_back(to_string(st.hourOfOpening));
        row.push_back(st.nameOfLine);
        return row;
    }

    static metroLine rowToMetroLine(const CsvRow &row)
    {
        metroLine instance;
        instance.id = stof(row[0]);
        instance.nameOfline = row[1];
        instance.amountOfstation = stoi(row[2]);
        instance.colourOfLine = row[3];
        return instance;
    }
    static CsvRow metroLineToRow(const metroLine &st)
    {
        CsvRow row;
        row.push_back(to_string(st.id));
        row.push_back(st.nameOfline);
        row.push_back(to_string(st.amountOfstation));
        row.push_back(st.colourOfLine);
        return row;
    }

    int getNewMetroStationId();
    int getNewMetroLineId();

  public:
    CsvStorage(const string &dir_name) : dir_name_(dir_name) {}
    bool open();
    bool close();
    // metroStations
    vector<metroStation> getAllmetroStations(void);
    optional<metroStation> getmetroStationById(int metroStation_id);
    bool updatemetroStation(const metroStation &metroStation, char ch);
    bool removemetroStation(int metroStation_id);
    int insertmetroStation(const metroStation &metroStation);

    //metroLines
    vector<metroLine> getAllmetroLines(void);
    optional<metroLine> getmetroLineById(int metroLine_id);
    bool updatemetroLine(const metroLine &metroLine,char ch);
    bool removemetroLine(int metroLine_id);
    int insertmetroLine(const metroLine &metroLine);
};
