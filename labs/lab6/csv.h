#pragma once

#include <vector>
#include <string>

using std::vector;
using std::string;

using CsvRow = vector<string>;   // type name alias
using CsvTable = vector<CsvRow>; // type name alias

namespace Csv
{
CsvTable createTableFromString(string &csv_str);
string createStringFromTable(CsvTable &csv_table);
} 









