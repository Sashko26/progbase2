#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>
class SqliteStorage : public Storage
{
    QSqlDatabase _db;
    const string dir_name_;
public:
    SqliteStorage(const string &dir_name) : dir_name_(dir_name)
    {
        _db = QSqlDatabase::addDatabase("QSQLITE");
    }



   bool open() ;
   bool close() ;
  // metroStations
   vector<metroStation> getAllmetroStations(void) ;
   optional<metroStation> getmetroStationById(int metroStation_id) ;
   bool updatemetroStation(const metroStation &metroStation, char ch) ;
   bool removemetroStation(int metroStation_id) ;
   int insertmetroStation(const metroStation &metroStation) ;
  // metroLine
   vector<metroLine> getAllmetroLines(void) ;
   optional<metroLine> getmetroLineById(int metroLine_id) ;
   bool updatemetroLine(const metroLine &metroLine, char ch) ;
   bool removemetroLine(int metroLine_id) ;
   int insertmetroLine(const metroLine &metroLine) ;
};

#endif // SQLITE_STORAGE_H
