#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include <stdbool.h>
#include <ctype.h>
#include <list.h>
#include <csv.h>




/* #include "list.h" */
struct __keyValue
{
    char *key;
    char *value;
};
typedef struct __keyValue keyValue;
struct __StrStrMap
{
    List *listKeyValue;
};
typedef struct __StrStrMap StrStrMap;

char *String_allocCopy(const char *value);
char *String_allocFromInt(int value);
char *String_allocFromDouble(double value);
StrStrMap *createStationMap(int orderLiteraInAlphabet, const char *nameOfStation, int hourOfOpening, const char *nameOfLine);
List *CreateTestListMap();

int station_OrderLiteraInAlphabet(StrStrMap *self);


void StrStrMap_init(StrStrMap *self);
void StrStrMap_deinit(StrStrMap *self);
StrStrMap *StrStrMap_alloc(void);
void StrStrMap_free(StrStrMap *self);
size_t StrStrMap_size(StrStrMap *self);
void StrStrMap_freeAndClearMapList(List* MapList);

void StrStrMap_add(StrStrMap *self, const char *key, const char *value);
bool StrStrMap_contains(StrStrMap *self, const char *key);
const char *StrStrMap_get(StrStrMap *self, const char *key);
const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value);
const char *StrStrMap_remove(StrStrMap *self, const char *key);
void StrStrMap_clear(StrStrMap *self);

void createTablefromListMapWithOptionN(List *table, int len, List *ListMap, int N);
void fromListMapToTable(List *table, List *ListMap, int len);
List *createListOfMapFromTable(List *Table);








