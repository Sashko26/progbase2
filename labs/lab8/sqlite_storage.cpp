#include "sqlite_storage.h"
#include <QtSql>
#include <QString>
#include <iostream>
#include <metroLine.h>
#include <QFile>

bool SqliteStorage::open()
{
    string path=this->dir_name_+"/data.sql";
     QString q_path=QString::fromStdString(path);
     QFile file("../lab8/file.txt");
     file.open(QIODevice::WriteOnly);

     file.write(static_cast<const char*>(path.c_str()));
     file.close();
    _db.setDatabaseName(q_path);    // set sqlite database file path
    bool connected = _db.open();  // open db connection
    if (!connected)
    {
        puts(path.c_str());
        puts("ne vidkrylo!");
        return false;
    }
      return true;
}
bool SqliteStorage::close()
{
    _db.close();
    return true;
}
// metroStations

vector<metroStation> SqliteStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations;
    QSqlQuery query("SELECT * FROM metro_stations");

    while (query.next())
    {
       metroStation station;
       QString station_name = query.value("station_name").toString();
       int id = query.value("id").toInt();
       int opening_time = query.value("opening_time").toInt();
       QString line_name = query.value("line_name").toString();
      // qDebug() << id << " | " << station_name; qdebug виводить
      //повідомлення в консоль, тому не дуже гарно, коли
      //програма консольна і виводяться технічні повідомлення
       station.id=id;
       station.nameOfStation=station_name.toStdString();
       station.nameOfLine=line_name.toStdString();
       station.hourOfOpening=opening_time;
       metroStations.push_back(station);
    }
    return metroStations;
}
optional<metroStation> SqliteStorage::getmetroStationById(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_station error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroStation station;
              QString station_name = query.value("station_name").toString();
              int id = query.value("id").toInt();
              int opening_time = query.value("opening_time").toInt();
              QString line_name = query.value("line_name").toString();
              qDebug() << id << " | " << station_name;
              station.id=id;
              station.nameOfStation=station_name.toStdString();
              station.nameOfLine=line_name.toStdString();
              station.hourOfOpening=opening_time;
            return station;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroStation(const metroStation &metroStation, char ch)
{

    QSqlQuery query;
    query.prepare("UPDATE metro_stations SET station_name = :station_name , opening_time = :opening_time, line_name = :line_name WHERE id = :id");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    cout<<metroStation.hourOfOpening<<endl;
    query.bindValue(":opening_time", metroStation.hourOfOpening);
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":id", metroStation.id);


     if (!query.exec())
    {
        qDebug() << "update station error:" << query.lastError();
        cout<<metroStation.hourOfOpening<<endl;
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
        puts("hi mazafucker");
        abort();
    }




    return true;
}
bool SqliteStorage::removemetroStation(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_station error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroStation(const metroStation &metroStation)
{

    QSqlQuery query;
    query.prepare("INSERT INTO metro_stations (station_name, opening_time, line_name) VALUES (:station_name, :opening_time, :line_name)");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":opening_time", metroStation.hourOfOpening);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();
}




// metroLine
vector<metroLine> SqliteStorage::getAllmetroLines(void)
{
    vector<metroLine> metroLines;
    QSqlQuery query("SELECT * FROM metro_lines");

    while (query.next())
    {
       metroLine line;
       QString line_name = query.value("line_name").toString();
       int id = query.value("id").toInt();
       int amount_stations = query.value("amount_stations").toInt();
       QString color_line= query.value("color_line").toString();
      // qDebug() << id << " | " << station_name; qdebug виводить
      //повідомлення в консоль, тому не дуже гарно, коли
      //програма консольна і виводяться технічні повідомлення
       line.id=id;
       line.nameOfline=line_name.toStdString();
       line.colourOfLine=color_line.toStdString();
       line.amountOfstation=amount_stations;
       metroLines.push_back(line);
    }
    return metroLines;

}
optional<metroLine> SqliteStorage::getmetroLineById(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_station error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroLine line;
              QString line_name = query.value("line_name").toString();
              int id = query.value("id").toInt();
              int amount_stations = query.value("amount_stations").toInt();
              QString color_line = query.value("color_line").toString();
              //qDebug() << id << " | " << line_name;
              line.id=id;
              line.nameOfline=line_name.toStdString();
              line.colourOfLine=color_line.toStdString();
              line.amountOfstation=amount_stations;
            return line;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroLine(const metroLine &metroLine, char ch)
{
    QSqlQuery query;
    query.prepare("UPDATE metro_lines SET line_name = :line_name, amount_stations = :amount_stations, color_line = :color_line WHERE id = :id");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":id", metroLine.id);
 if (!query.exec())
    {
        qDebug() << "update Line error:" << query.lastError();
        return false;

    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }


    return true;
}
bool SqliteStorage::removemetroLine(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_line error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroLine(const metroLine &metroLine)
{
    QSqlQuery query;
    query.prepare("INSERT INTO metro_lines (line_name, amount_stations, color_line) VALUES (:line_name, :amount_stations, :color_line)");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();

}
