#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "metroStation.h"
#include "metroLine.h"

using namespace std;

class Storage
{
 public:
    //virtual ~Storage(){}
   virtual bool open() = 0;
   virtual bool close() = 0;
   // metroStations
   virtual vector<metroStation> getAllmetroStations(void) = 0;
   virtual optional<metroStation> getmetroStationById(int metroStation_id) = 0;
   virtual bool updatemetroStation(const metroStation &metroStation, char ch) = 0;
   virtual bool removemetroStation(int metroStation_id) = 0;
   virtual int insertmetroStation(const metroStation &metroStation) = 0;
   // metroLine
   virtual vector<metroLine> getAllmetroLines(void) = 0;
   virtual optional<metroLine> getmetroLineById(int metroLine_id) = 0;
   virtual bool updatemetroLine(const metroLine &metroLine,char ch) = 0;
   virtual bool removemetroLine(int metroLine_id) = 0;
   virtual int insertmetroLine(const metroLine &metroLine) = 0;
};
