#include "csv_storage.h"
using namespace std;

bool CsvStorage::open()
{
    string row_str;
    string table_str;
    ifstream fp;
    string filename = this->dir_name_ + "/metroStation.csv";
    fp.open(filename);
    if (!fp.good())
    {
        return false;
    }
    while (getline(fp, row_str))
    {
        table_str += row_str + '\n';
    }
    table_str[table_str.size() - 1] = '\0';
    CsvTable table = Csv::createTableFromString(table_str);
    for (int i = 1; i < static_cast<int>(table.size()); i++)
    {
        CsvRow &ref = table[i];
        metroStation station = rowToMetroStation(ref);
        this->metroStations_.push_back(station);
    }
    fp.close();

    /// line
    string row_str_line;
    string table_str_line;

    ifstream fp_line;
    string filename_line = this->dir_name_ + "/metroLines.csv";
    fp_line.open(filename_line);
    if (!fp_line.good())
    {
        cerr << "Файл не вдалося відкрити" << endl;
        return false;
    }

    while (getline(fp_line, row_str_line))
    {
        table_str_line += row_str_line + '\n';
    }

    table_str_line[table_str_line.size() - 1] = '\0';
    CsvTable table_line = Csv::createTableFromString(table_str_line);
    for (int i = 1; i <static_cast<int>(table_line.size()); i++)
    {
        CsvRow &ref = table_line[i];
        metroLine line = rowToMetroLine(ref);
        this->metroLines_.push_back(line);
    }
    fp_line.close();
    fp.close();
    return true;
}

// interesting записування тексту у файл
bool CsvStorage::close()
{
    CsvTable table;
    CsvRow row;
    row.push_back("id");
    row.push_back("Назва станції");
    row.push_back("Година відкриття");
    row.push_back("Назва лінії");
    table.push_back(row);
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {

        CsvRow row = metroStationToRow(this->metroStations_[i]);
        table.push_back(row);
    }
    CsvTable &ref = table;
    string str = Csv::createStringFromTable(ref);
    ofstream outfile;
    string fileName = this->dir_name_ + "/metroStation.csv";
    outfile.open(fileName);
    if (!outfile.is_open())
    {
        cerr << "файл не вдалось відкрити!" << endl;
        abort();
    }
    outfile << str;
    outfile.close();

    /// line
    CsvTable table_line;
    CsvRow row_line;
    row_line.push_back("id");
    row_line.push_back("Назва лінії");
    row_line.push_back("Кількість станцій");
    row_line.push_back("Колір лінії");
    table_line.push_back(row_line);
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        CsvRow row_line = metroLineToRow(this->metroLines_[i]);
        table_line.push_back(row_line);
    }

    string str_line = Csv::createStringFromTable(table_line);
    ofstream outfile_line;
    string fileName_line = this->dir_name_ + "/metroLines.csv";
    outfile_line.open(fileName_line);
    if (!outfile_line.is_open())
    {
        cerr << "файл не вдалось відкрити!" << endl;
        abort();
    }
    outfile_line << str_line;
    outfile_line.close();

    return true;
}

////////////metroStation
vector<metroStation> CsvStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations = this->metroStations_;
    return metroStations;
}
bool CsvStorage::updatemetroStation(const metroStation &metroStation, char ch)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation.id == this->metroStations_[i].id)
        {
            if (ch == '1')
                this->metroStations_[i].nameOfStation = metroStation.nameOfStation;
            else if (ch == '2')
                this->metroStations_[i].nameOfLine = metroStation.nameOfLine;
            else if (ch == '3')
                this->metroStations_[i].hourOfOpening = metroStation.hourOfOpening;
            this->close();
            return true;
        }
    }
    return false;
}
bool CsvStorage::removemetroStation(int metroStation_id)
{
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (metroStation_id == this->metroStations_[i].id)
        {
            this->metroStations_.erase(this->metroStations_.begin() + i);
            this->close();
            return true;
        }
    }
    return false;
}

int CsvStorage::insertmetroStation(const metroStation &metroStation)
{
    struct metroStation copy;
    copy.hourOfOpening = metroStation.hourOfOpening;
    copy.nameOfLine = metroStation.nameOfLine;
    copy.nameOfStation = metroStation.nameOfStation;
    int id = getNewMetroStationId();
    copy.id = id;
    this->metroStations_.push_back(copy);
    this->close();
    return id;
}
int CsvStorage::getNewMetroStationId()
{
    ifstream fp;
    string filename = this->dir_name_ + "/id.txt";
    fp.open(filename);
    if (!fp.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    string idStr;
    getline(fp, idStr);
    int id = stoi(idStr);
    fp.close();
    ofstream fout;
    fout.open(filename);
    if (!fout.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    id++;
    idStr = to_string(id);
    fout << idStr;
    return id;
}

optional<metroStation> CsvStorage::getmetroStationById(int metroStation_id)
{
    optional<metroStation> optStation;
    for (int i = 0; i < static_cast<int>(this->metroStations_.size()); i++)
    {
        if (this->metroStations_[i].id == metroStation_id)
        {
            optStation = metroStations_[i];
            return optStation;
        }
    }
    return optStation;
}

////metroLine

vector<metroLine> CsvStorage::getAllmetroLines(void)
{
    vector<metroLine> metroLines = this->metroLines_;
    return metroLines;
}
optional<metroLine> CsvStorage::getmetroLineById(int metroLine_id)
{
    optional<metroLine> optStation;
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (this->metroLines_[i].id == metroLine_id)
        {
            optStation = metroLines_[i];
            return optStation;
        }
    }
    return optStation;
}
bool CsvStorage::updatemetroLine(const metroLine &metroLine, char ch)
{
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (metroLine.id == this->metroLines_[i].id)
        {
            if (ch == '1')
            {
                puts("hi");
                this->metroLines_[i].nameOfline = metroLine.nameOfline;
            }
            else if (ch == '2')
            {
                puts("hi");
                this->metroLines_[i].amountOfstation = metroLine.amountOfstation;
            }

            else if (ch == '3')
            {
                puts("hi");
                this->metroLines_[i].colourOfLine = metroLine.colourOfLine;
            }

            this->close();
            return true;
        }
    }
    return false;
}
bool CsvStorage::removemetroLine(int metroLine_id)
{
    for (int i = 0; i < static_cast<int>(this->metroLines_.size()); i++)
    {
        if (metroLine_id == this->metroLines_[i].id)
        {
            this->metroLines_.erase(this->metroLines_.begin() + i);
            this->close();
            return true;
        }
    }
    return false;
}
int CsvStorage::insertmetroLine(const metroLine &metroLine)
{
    struct metroLine copy;
    copy.nameOfline = metroLine.nameOfline;
    copy.amountOfstation = metroLine.amountOfstation;
    copy.colourOfLine = metroLine.colourOfLine;
    int id = getNewMetroLineId();
    copy.id = id;
    this->metroLines_.push_back(copy);
    this->close();
    return id;
}
int CsvStorage::getNewMetroLineId()
{
    ifstream fp;
    string filename = this->dir_name_ + "/lineId.txt";
    fp.open(filename);
    if (!fp.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    string idStr;
    getline(fp, idStr);
    int id = stoi(idStr);
    fp.close();
    ofstream fout;
    fout.open(filename);
    if (!fout.good())
    {
        cerr << "файл не відкрився!" << endl;
        abort();
    }
    id++;
    idStr = to_string(id);
    fout << idStr;
    return id;
}
