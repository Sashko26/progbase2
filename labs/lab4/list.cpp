#include "list.hpp"
using namespace std;
float &DynArray::operator[](int index)
{
    if (index >= this->_length || index < 0)
    {
        std::cout << "Неправильно заданий індекс!" << std::endl;
        abort();
    }
    return this->_array[index];
}
void DynArray::enlarge(int newCapacity) 
  {
    float *newArray = static_cast<float *>(realloc(this->_array, sizeof(float) * newCapacity));
    if (newArray == NULL)
    {
      delete[] this->_array;
      cout << "Reallocation error" << endl;
      abort();
    }
    this->_array = newArray;
    this->_capacity = newCapacity;
  }
// ініціалізація динамічного масиву
DynArray::DynArray()
{
    this->_length = 0;
    this->_capacity = 4;
    this->_array = static_cast<float *>(malloc(sizeof(float) * this->_capacity));
    if (this->_array == NULL)
    {
        cout << "Allocation error" << endl;
        abort();
    }
}
// видалення диначічного масиву
DynArray::~DynArray()
{
    free(this->_array);
}
// перевиділення пам'яті для динамічного масиву

void DynArray::add(float value)
{
    this->_array[this->_length] = value;
    //
    this->_length += 1;
    //
    if (this->_length == this->_capacity)
    {
        // realloc
        int newCapacity = this->_capacity * 2;
        enlarge(newCapacity);
    }
}

void DynArray::print()
{
    for (int i = 0; i < this->_length; i++)
    {
        float value = this->_array[i];
        cout << value << ", ";
    }
    cout << endl;
}
// отримання значення за індексом
float DynArray::getValueByIndex(int index)
{
    float val = this->_array[index];
    return val;
}

// отримання значення розміру списку
int DynArray::size()
{
    int val = this->_length;
    return val;
}

// fuction that not pass for conseption ATD
void DynArray::fromStringToList(string &str, const int N)
{
    string helpStr("", N);
    int length = str.length();
    for (int i = 0; i < length;)
    {
        if (isdigit(str[i]))
        {
            int k = 0;
            while (isdigit(str[i]) || str[i] == '.')
            {
                helpStr[k] = str[i];
                k++;
                i++;
            }
            helpStr[k] = '\0';
            this->add(atof(helpStr.c_str()));
        }
        else
        {
            i++;
        }
    }
}
void DynArray::zeroFractionAtBegin()
{
    for (int i = 1; i < this->size(); i++)
    {
        int j = i;
        while (j != 0)
        {
            if (fabs(this->getValueByIndex(j) - static_cast<int>(this->getValueByIndex(j))) < 0.000001 && fabs(this->getValueByIndex(j - 1) - static_cast<int>(this->getValueByIndex(j - 1))) > 0.000001)
            {
                int buf = this->_array[j];
                this->_array[j] = this->_array[j - 1];
                this->_array[j - 1] = buf;
            }
            j--;
        }
    }
}
