#pragma once
#include "list.h"
void Csv_addRow(List *table, List *row);
void Csv_addInt(List *row, int value);
void Csv_addFloat(List *row, float value);
void Csv_addString(List *row, const char *value);
void Csv_fillTableFromString(List *csvTable, const char *csvString);
char *Csv_fillStringFromTable(List *csvTable);
char *Csv_fillStringFromList(List *Row);
int Csv_int(List *row, int index);
double Csv_double(List *row, int index);
int Csv_string(List *row, int index, char *b, int n);
void Csv_addRow(List *table, List *row);
List *Csv_row(List *table, int index);
void Csv_clearTable(List *csvTable);





