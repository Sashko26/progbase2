#include "list.hpp"
#include "deque.hpp"
#include <fstream>
int main()
{
    ifstream file;
    file.open("data.txt");
    if (!file)
    {
        cout << "проблема з файлом!" << endl;
    }
    std::string str;
    getline(file, str);
    file.close();
    const int N = 1000;
    //ініціалізація динамічного масиву
    DynArray list;
    cout << str << endl;
    cout << "|" << str.length() << "|" << endl;
    file.close();
    list.fromStringToList(str, N);
    cout << "cписок, який складається з чисел текстового файлу:" << std::endl;
    list.print();
    list.zeroFractionAtBegin();
    cout << "відсортований cписок, у якому елементи з нульовим дробом на початку:" << std::endl;
    list.print();
    Deque firstDeque;
    Deque secondDeque;

    for (int i = 0; i < list.size(); i++)
    {
        if (i % 2 == 0)
        {
            firstDeque.pushByFront(list.getValueByIndex(i));
        }
        else
        {
            secondDeque.pushByRear(list.getValueByIndex(i));
        }
    }
    cout << "перша дека, у якій всі елементи парних позицій елементи відсортованого списку були додані на початок:" << std::endl;
    firstDeque.print();
    cout << "друга дека, у якій всі елементи непарних позицій відсортованого списку були додані в кінець:" << std::endl;
    secondDeque.print();
    DynArray newList;
    for (int i = firstDeque.size() - 1; i >= 0; i--)
    {
        newList.add(firstDeque.popByFront());
    }
    for (int i = secondDeque.size() - 1; i >= 0; i--)
    {
        newList.add(secondDeque.popByRear());
    }
    cout << "новий список, у якому переписані всі елементи з початку першої деки і з кінця другої:" << endl;
    newList.print();
    return 0;
}
