/********************************************************************************
** Form generated from reading UI file 'authentication.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHENTICATION_H
#define UI_AUTHENTICATION_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>

QT_BEGIN_NAMESPACE

class Ui_authentication
{
public:
    QDialogButtonBox *buttonBox;
    QLineEdit *lineEdit_login;
    QLineEdit *lineEdit_password;
    QLabel *login;
    QLabel *password;
    QLabel *label_invorrect;

    void setupUi(QDialog *authentication)
    {
        if (authentication->objectName().isEmpty())
            authentication->setObjectName(QStringLiteral("authentication"));
        authentication->resize(400, 300);
        buttonBox = new QDialogButtonBox(authentication);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 240, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        lineEdit_login = new QLineEdit(authentication);
        lineEdit_login->setObjectName(QStringLiteral("lineEdit_login"));
        lineEdit_login->setGeometry(QRect(190, 150, 113, 25));
        lineEdit_password = new QLineEdit(authentication);
        lineEdit_password->setObjectName(QStringLiteral("lineEdit_password"));
        lineEdit_password->setGeometry(QRect(190, 190, 113, 25));
        login = new QLabel(authentication);
        login->setObjectName(QStringLiteral("login"));
        login->setGeometry(QRect(150, 150, 41, 17));
        password = new QLabel(authentication);
        password->setObjectName(QStringLiteral("password"));
        password->setGeometry(QRect(120, 190, 71, 20));
        label_invorrect = new QLabel(authentication);
        label_invorrect->setObjectName(QStringLiteral("label_invorrect"));
        label_invorrect->setGeometry(QRect(90, 100, 67, 17));

        retranslateUi(authentication);
        QObject::connect(buttonBox, SIGNAL(accepted()), authentication, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), authentication, SLOT(reject()));

        QMetaObject::connectSlotsByName(authentication);
    } // setupUi

    void retranslateUi(QDialog *authentication)
    {
        authentication->setWindowTitle(QApplication::translate("authentication", "Authntication", Q_NULLPTR));
        login->setText(QApplication::translate("authentication", "login:", Q_NULLPTR));
        password->setText(QApplication::translate("authentication", "password:", Q_NULLPTR));
        label_invorrect->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class authentication: public Ui_authentication {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHENTICATION_H
