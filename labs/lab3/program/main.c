#include <unistd.h>
#include <getopt.h>


#include <list.h>
#include <csv.h>
#include "Table.h"
#include "StrStrMap.h"
#include "BSTree.h"
#include "StrStrMap.h"


void whenWeHaveInputFile(char *inputFile, List *Table, const char *state);
void whenOptionNandOptionBisTrue(List *listOfMap, char *valueN);
void whenNisfalse(List *listOfMap);
int main(int argc, char *argv[])
{

    List *testListMap = NULL;
    testListMap = CreateTestListMap();
    int lenTest = 8;
    int opt;
    bool optionO = false;
    char valueO[100];
    bool optionN = false;
    bool optionB = false;
    bool extraArg = false;
    char valueN[100] = "\0";
    char extraArgument[100] = "\0";

    while ((opt = getopt(argc, argv, "n:o:b")) != -1)
    {

        switch (opt)
        {
        case 'n':
            printf("option: %c, value: %s\n", opt, optarg);
            optionN = true;
            strcpy(valueN, optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            optionO = true;
            strcpy(valueO, optarg);
            break;
        case 'b':
            printf("option: %c\n", opt);
            optionB = true;
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }
    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);
        extraArg = true;
        strcpy(extraArgument, argv[optind]);
    }
    if (extraArg == true && optionO == false && optionN == false && (optionB == false || optionB == true))
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        List *listOfMap = createListOfMapFromTable(&Table);
        printStringsTable(&Table);
        if (optionB == true)
        {
            whenNisfalse(listOfMap);
        }
        deinitTable(&Table);
        StrStrMap_freeAndClearMapList(testListMap);
        StrStrMap_freeAndClearMapList(listOfMap);
    }
    //work
    else if (extraArg == false && optionO == true && optionN == false && (optionB == false || optionB == true))
    {
        List table;
        List_init(&table);
        printf("%s\n", valueO);
        FILE *fp;
        fp = fopen(valueO, "w");
        fromListMapToTable(&table, testListMap, lenTest);
        char *csvString = Csv_fillStringFromTableModify(&table);
        puts(csvString);
        fputs(csvString, fp);
        free(csvString);
        if (optionB == true)
        {
            whenNisfalse(testListMap);
        }
        deinitTable(&table);
        fclose(fp);
        StrStrMap_freeAndClearMapList(testListMap);
    }
    //work
    else if (extraArg == false && optionO == false && optionN == true && (optionB == false || optionB == true))
    {
        List table;
        List_init(&table);
        createTablefromListMapWithOptionN(&table, lenTest + 1, testListMap, atoi(valueN));
        printStringsTable(&table);
        if (optionB == true)
        {
            whenOptionNandOptionBisTrue(testListMap, valueN);
        }
        deinitTable(&table);
        StrStrMap_freeAndClearMapList(testListMap);
    }

    //працює
    else if (extraArg == false && optionO == false && optionN == false && (optionB == false || optionB == true))
    {
        List table;
        List_init(&table);
        fromListMapToTable(&table, testListMap, lenTest + 1);
        printStringsTable(&table);
        if (optionB == true)
        {
            whenNisfalse(testListMap);
        }
        deinitTable(&table);
        StrStrMap_freeAndClearMapList(testListMap);
    }

    //work
    else if (extraArg == true && optionO == true && optionN == false && (optionB == false || optionB == true))
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        printStringsTable(&Table);
        char *csv_string = Csv_fillStringFromTableModify(&Table);
        FILE *fp1;
        fp1 = fopen(valueO, "w");
        fputs(csv_string, fp1);
        free(csv_string);
        List *listOfMap = createListOfMapFromTable(&Table);
        if (optionB == true)
        {
            whenNisfalse(listOfMap);
        }
        StrStrMap_freeAndClearMapList(listOfMap);
        fclose(fp1);
        deinitTable(&Table);
        StrStrMap_freeAndClearMapList(testListMap);
    }

    //work
    else if (extraArg == true && optionO == false && optionN == true && (optionB == false || optionB == true))
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        List *listOfMap = createListOfMapFromTable(&Table);
        int N = atoi(valueN);
        List table = createTable();
        int len = List_size(&Table);
        createTablefromListMapWithOptionN(&table, len, listOfMap, N); //Другий випадок використання Len (несостиковка з заголовочним рядком, який в);
        printStringsTable(&table);
        if (optionB == true)
        {
            whenOptionNandOptionBisTrue(listOfMap, valueN);
        }
        deinitTable(&Table);
        deinitTable(&table);
        StrStrMap_freeAndClearMapList(listOfMap);
        StrStrMap_freeAndClearMapList(testListMap);
    }
    //work
    else if (extraArg == false && optionO == true && optionN == true && (optionB == false || optionB == true))
    {
        List table = createTable();
        printf("%s\n", valueO);
        FILE *fp;
        fp = fopen(valueO, "w");
        createTablefromListMapWithOptionN(&table, lenTest + 1, testListMap, atoi(valueN)); //цікаво, подивись на реалізацію
        char *csvString = Csv_fillStringFromTableModify(&table);
        puts(csvString);
        fputs(csvString, fp);
        free(csvString);
        if (optionB == true)
        {
            whenOptionNandOptionBisTrue(testListMap, valueN);
        }
        deinitTable(&table);
        fclose(fp);
        StrStrMap_freeAndClearMapList(testListMap);
    }

    //work
    else if (extraArg == true && optionO == true && optionN == true && (optionB == false || optionB == true))
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        List *listOfMap = createListOfMapFromTable(&Table);
        List table = createTable();
        createTablefromListMapWithOptionN(&table, lenTest + 1, listOfMap, atoi(valueN));
        printStringsTable(&table);
        char *Csv_string = Csv_fillStringFromTableModify(&table);
        FILE *fp1;
        fp1 = fopen(valueO, "w");
        fputs(Csv_string, fp1);
        fclose(fp1);
        free(Csv_string);
        if (optionB == true)
        {
            whenOptionNandOptionBisTrue(listOfMap, valueN);
        }
        deinitTable(&Table);
        deinitTable(&table);
        StrStrMap_freeAndClearMapList(testListMap);
        StrStrMap_freeAndClearMapList(listOfMap);
    }
    return 0;
}

void whenWeHaveInputFile(char *inputFile, List *Table, const char *state)
{
    FILE *fp = NULL;
    fp = fopen(inputFile, state);
    if (fp == NULL)
    {
        abort();
    }
    int index = -1;
    const int LEN = 100000;
    char *str = malloc(sizeof(char) * LEN);
    do
    {
        index++;
        str[index] = fgetc(fp);
    } while (str[index] != EOF);
    str[index] = '\0';
    Csv_fillTableFromStringModify(Table, str);

    fclose(fp);
    free(str);
}
void whenOptionNandOptionBisTrue(List *listOfMap, char *valueN)
{
    BSTree *Tree = createBSTreeFromlistMap(listOfMap);
    printTree(BStree_getRoot(Tree));
    printf("Виведемо оновлене дерево з опцією -n:\n");
    deleteElementsFromBSTreeWithoptionN(listOfMap, Tree, atoi(valueN));
    printTree(BStree_getRoot(Tree));
    BSTree_free(Tree);
}
void whenNisfalse(List *listOfMap)
{
    BSTree *Tree = createBSTreeFromlistMap(listOfMap);
    printTree(BStree_getRoot(Tree));
    BSTree_free(Tree);
}
