#include "StrStrMap.h"
/* #include "list.h"
#include "csv.h" */
char *String_allocCopy(const char *value)
{
    char *heapValue = malloc(sizeof(char) * 100);
    strcpy(heapValue, value);
    return heapValue;
};
char *String_allocFromInt(int value)
{
    char *fillStringFromInt = malloc(sizeof(char) * 100);
    sprintf(fillStringFromInt, "%i", value);
    return fillStringFromInt;
}
char *String_allocFromDouble(double value)
{
    char *fillStringFromInt = malloc(sizeof(char) * 100);
    sprintf(fillStringFromInt, "%lf", value);
    return fillStringFromInt;
}
int station_OrderLiteraInAlphabet(StrStrMap *self)
{
    int key;
    key = atoi(StrStrMap_get(self, "orderLiteraInAlphabet"));
    return key;
}

StrStrMap *createStationMap(int orderLiteraInAlphabet, const char *nameOfStation, int hourOfOpening, const char *nameOfLine)
{
    // create values copies as strings on heap
    char *idValue = String_allocFromInt(orderLiteraInAlphabet);
    char *fullnameValue = String_allocCopy(nameOfStation);
    char *hourOfOpeningValue = String_allocFromInt(hourOfOpening);
    char *nameOfLineValue = String_allocCopy(nameOfLine);
    // create map on heap and add values
    StrStrMap *map = StrStrMap_alloc();
    StrStrMap_add(map, "orderLiteraInAlphabet", idValue);
    StrStrMap_add(map, "nameOfStation", fullnameValue);
    StrStrMap_add(map, "hourOfOpening", hourOfOpeningValue);
    StrStrMap_add(map, "nameOfLine", nameOfLineValue);

    // test checks
    assert(StrStrMap_contains(map, "orderLiteraInAlphabet"));
    assert(StrStrMap_contains(map, "nameOfStation"));
    assert(StrStrMap_contains(map, "hourOfOpening"));
    assert(StrStrMap_contains(map, "nameOfLine"));
    free(idValue);
    free(fullnameValue);
    free(hourOfOpeningValue);
    free(nameOfLineValue);
    return map;
}

StrStrMap *StrStrMap_alloc(void)
{
    StrStrMap *self = NULL;
    self = malloc(sizeof(StrStrMap));
    StrStrMap_init(self);
    return self;
}

void StrStrMap_free(StrStrMap *self)
{
    StrStrMap_deinit(self);
    free(self);
}

void StrStrMap_init(StrStrMap *self)
{
    self->listKeyValue = List_alloc();
}
void StrStrMap_deinit(StrStrMap *self)
{
    List_free(self->listKeyValue);
}

void StrStrMap_add(StrStrMap *self, const char *key, const char *value)
{
    if (StrStrMap_contains(self, key) == true)
    {
        fprintf(stderr, "Такий ключ вже існує!\n");
    }
    else
    {
        keyValue *KV = malloc(sizeof(keyValue));
        KV->key = malloc(sizeof(char) * 100);
        KV->value = malloc(sizeof(char) * 100);
        strcpy(KV->key, key);
        strcpy(KV->value, value);
        List_add(self->listKeyValue, KV);
    }
}
bool StrStrMap_contains(StrStrMap *self, const char *key)
{
    for (int i = 0; i < List_size(self->listKeyValue); i++)
    {
        keyValue *pKV = self->listKeyValue->items[i];
        if (strcmp(pKV->key, key) == 0)
        {
            return true;
        }
    }
    return false;
}

void StrStrMap_clear(StrStrMap *self)
{
    for (int i = 0; i < List_size(self->listKeyValue); i++)
    {
        keyValue *pKV = self->listKeyValue->items[i];
        free(pKV->key);
        free(pKV->value);
        free(pKV);
    }
    self->listKeyValue->size = 0;
}
size_t StrStrMap_size(StrStrMap *self)
{
    size_t size = List_size(self->listKeyValue);
    return size;
}
const char *StrStrMap_get(StrStrMap *self, const char *key)
{
    if (StrStrMap_contains(self, key) == false)
    {
        puts(key);
        fprintf(stderr, "Такого ключа в мапі не існує!");
        return NULL;
    }
    else
    {
        for (int i = 0; i < StrStrMap_size(self); i++)
        {
            if (strcmp(((keyValue *)self->listKeyValue->items[i])->key, key) == 0)
            {
                return ((keyValue *)self->listKeyValue->items[i])->value;
            }
        }
    }
    return NULL;
}
const char *StrStrMap_set(StrStrMap *self, const char *key, const char *value)
{
    if (StrStrMap_contains(self, key) == false)
    {

        fprintf(stderr, "Такого ключа в мапі не існує!");
        return NULL;
    }
    for (int i = 0; i < StrStrMap_size(self); i++)
    {
        if (strcmp(((keyValue *)self->listKeyValue->items[i])->key, key) == 0)
        {
            char *old = malloc(sizeof(char) * 100);
            strcpy(old, ((keyValue *)self->listKeyValue->items[i])->value);
            strcpy(((keyValue *)self->listKeyValue->items[i])->value, value);
            return old;
        }
    }
    return NULL;
}
void StrStrMap_freeAndClearMapList(List *MapList)
{
    for (int i = 0; i < List_size(MapList); i++)
    {
        StrStrMap_clear(MapList->items[i]);
        StrStrMap_free(MapList->items[i]);
    }
    List_free(MapList);
}

List *CreateTestListMap()
{
    List *testListMap = List_alloc();
    StrStrMap *Map1 = createStationMap(26, "Хрещатик", 4, "Святошинсько-Броварська лінія");
    StrStrMap *Map2 = createStationMap(20, "Політехнічний інститут", 8, "Святошинсько-Броварська лінія");
    StrStrMap *Map3 = createStationMap(6, "Деміївська", 12, "Оболонсько-Теремківська лінія");
    StrStrMap *Map4 = createStationMap(23, "Теремки", 18, "Оболонсько-Теремківська лінія");
    StrStrMap *Map5 = createStationMap(10, "Золоті Ворота", 21, "Сирецько-Печерська лінія");
    StrStrMap *Map6 = createStationMap(29, "Шулявська", 15, "Святошинсько-Броварська лінія");
    StrStrMap *Map7 = createStationMap(3, "Васильківська", 24, "Святошинсько-Броварська лінія");
    StrStrMap *Map8 = createStationMap(4, "Голосіївська", 13, "Оболонсько-Теремківська лінія");
    List_add(testListMap, Map1);
    List_add(testListMap, Map2);
    List_add(testListMap, Map3);
    List_add(testListMap, Map4);
    List_add(testListMap, Map5);
    List_add(testListMap, Map6);
    List_add(testListMap, Map7);
    List_add(testListMap, Map8);
    assert(List_contains(testListMap, Map1) == true);
    assert(List_contains(testListMap, Map2) == true);
    assert(List_contains(testListMap, Map3) == true);
    assert(List_contains(testListMap, Map4) == true);
    assert(List_contains(testListMap, Map5) == true);
    assert(List_contains(testListMap, Map6) == true);
    assert(List_contains(testListMap, Map7) == true);
    assert(List_contains(testListMap, Map8) == true);
    return testListMap;
}
void createTablefromListMapWithOptionN(List *table, int len, List *ListMap, int N)
{
    for (int i = 0; i < len; i++)
    {
        if (i == 0)
        {
            List *pRow = List_alloc();
            Csv_addString(pRow, "Порядок букви в алфавіті");
            Csv_addString(pRow, "Назва Станції");
            Csv_addString(pRow, "година відкриття");
            Csv_addString(pRow, "Назва лінії");
            Csv_addRow(table, pRow);
        }
        else if (atoi(StrStrMap_get(ListMap->items[i - 1], "hourOfOpening")) < N)
        {
            StrStrMap *pMap = (StrStrMap *)ListMap->items[i - 1];
            List *pRow = List_alloc();
            Csv_addString(pRow, StrStrMap_get(pMap, "orderLiteraInAlphabet"));
            Csv_addString(pRow, StrStrMap_get(pMap, "nameOfStation"));
            Csv_addString(pRow, StrStrMap_get(pMap, "hourOfOpening"));
            Csv_addString(pRow, StrStrMap_get(pMap, "nameOfLine"));
            Csv_addRow(table, pRow);
        }
    }
}
void fromListMapToTable(List *table, List *ListMap, int len)
{
    for (int i = 0; i < len ; i++)
    {
        if (i == 0)
        {
            List *pRow = List_alloc();
            Csv_addString(pRow, "Порядок букви в алфавіті");
            Csv_addString(pRow, "Назва Станції");
            Csv_addString(pRow, "година відкриття");
            Csv_addString(pRow, "Назва лінії");
            Csv_addRow(table, pRow);
        }
        else
        {
            StrStrMap *pMap = (StrStrMap *)ListMap->items[i - 1];
            List *pRow = List_alloc();
            Csv_addString(pRow, StrStrMap_get(pMap, "orderLiteraInAlphabet"));
            Csv_addString(pRow, StrStrMap_get(pMap, "nameOfStation"));
            Csv_addString(pRow, StrStrMap_get(pMap, "hourOfOpening"));
            Csv_addString(pRow, StrStrMap_get(pMap, "nameOfLine"));
            Csv_addRow(table, pRow);
        }
    }
}

List *createListOfMapFromTable(List *Table)
{
    List *ListOfMap = List_alloc();
    for (int i = 1; i < List_size(Table); i++)
    {
        if (List_size(Table->items[i]) != 4)
        {
            fprintf(stderr, "Таблиця заповнена неправильно(csv.c 65 string)");
            abort();
        }
        int j = 0;
        int orderLiteraInAlphabet = atoi((char *)((List *)(Table->items[i]))->items[j]);
        char nameOfStation[1000] = "\0";
        strcpy(nameOfStation, ((List *)(Table->items[i]))->items[j + 1]);
        int hourOfOpening = atoi(((List *)(Table->items[i]))->items[j + 2]);
        char nameOfLine[1000] = "\0";
        strcpy(nameOfLine, ((List *)(Table->items[i]))->items[j + 3]);
        StrStrMap *Map = createStationMap(orderLiteraInAlphabet, nameOfStation, hourOfOpening, nameOfLine);
        List_add(ListOfMap, Map);
    }
    return ListOfMap;
}
