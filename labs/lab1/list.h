#pragma once 
#include <stdlib.h>


typedef struct __DynArray DynArray;
struct __DynArray
{
    float *array;
    int capacity;
    int length;
};
void DynArray_init(DynArray *self);
void DynArray_deinit(DynArray *self);
void DynArray_add(DynArray *self, float value);
int  DynArray_size(DynArray *self);
float DynArray_getValueByIndex(DynArray *self,int index);
//
void DynArray_zeroFractionAtBegin(DynArray *self);
void DynArray_fromStringToArray(DynArray *self, char *string, int N);
void DynArray_print(const DynArray *self);