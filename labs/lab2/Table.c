#include"Table.h"
void deinitTable(List *p)
{
    for (int i = 0; i < List_size(p); i++)
    {
        List *row = List_get(p, i);
        for (int j = 0; j < List_size(row); j++)
        {
            char *str = List_get(row, j);
            free(str);
        }
        List_free(row);
    }
    List_deinit(p);
}

char *createOnHeap(const char *str)
{
    char *meme = malloc(sizeof(char) * (strlen(str) + 1));
    strcpy(meme, str);
    return meme;
}

List createTable()
{
    List table;
    List_init(&table);
    return table;
}

void printStringsTable(List *pl)
{
    for (int i = 0; i < List_size(pl); i++)
    {
        List *row = List_get(pl, i);
        printStrings(row);
    }
}

void printStrings(List *pl)
{
    for (int i = 0; i < List_size(pl); i++)
    {
        char *item = List_get(pl, i);
        printf("\"%s\", ", item);
    }
    printf("\n");
}