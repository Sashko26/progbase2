#include "csv_storage.h"
#include "xml_storage.h"
#include "cui.h"
using namespace std;

int main()
{
    bool workProgram=true;
    while(workProgram)
    {
        system("clear");
        cout<<"виберыть формат, з яким будете працювати:"<<endl;
        puts("1.xml");
        puts("2.csv");
        puts("3.exit");
        char ch;
        cin>>ch;
        if(ch!='1' && ch!='2' && ch!='3')
        {
              cout<<"введений некоректний символ! Спробуйте ще раз"<<endl;
              cin>>ch;
        }
        if(ch=='1')
        {
            XmlStorage xml_storage("../lab6/data/xml");
             Storage * storage_ptr =&xml_storage;
             storage_ptr->load();
             Cui cui(storage_ptr);
             cui.show();

        }
        else if(ch=='2')
        {
            CsvStorage csv_storage("../lab6/data/csv");
            Storage * storage_ptr =&csv_storage;
             storage_ptr->load();
             Cui cui(storage_ptr);
             cui.show();
        }
        else if(ch=='3')
        {
            workProgram=false;
        }

    }
 return 0;
}

