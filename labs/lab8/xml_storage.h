#pragma once

#include <vector>
#include <string>
#include "optional.h"
#include "metroStation.h"
#include "metroLine.h"
#include <fstream>
#include <iostream>
#include <progbase/console.h>
#include <progbase.h>
#include <cstdio>
#include <string.h>
#include "storage.h"

using std::string;
using std::vector;
using namespace std;
class XmlStorage : public Storage
{
    const string dir_name_;
    vector<metroStation> metroStations_;
    vector<metroLine> metroLines_;
    int getNewMetroStationId();
    int getNewMetroLineId();

  public:
    XmlStorage(const string &dir_name) : dir_name_(dir_name) {}
    bool open();
    bool close();
    // metroStations
    vector<metroStation> getAllmetroStations(void);
    optional<metroStation> getmetroStationById(int metroStation_id);
    bool updatemetroStation(const metroStation &metroStation, char ch);
    bool removemetroStation(int metroStation_id);
    int insertmetroStation(const metroStation &metroStation);

    //metroLines
    vector<metroLine> getAllmetroLines(void);
    optional<metroLine> getmetroLineById(int metroLine_id);
    bool updatemetroLine(const metroLine &metroLine, char ch);
    bool removemetroLine(int metroLine_id);
    int insertmetroLine(const metroLine &metroLine);
};
