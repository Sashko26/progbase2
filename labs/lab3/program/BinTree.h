#pragma once
#include <stdio.h>
#include "StrStrMap.h"
typedef struct __BinTree BinTree;
struct __BinTree 
{
   int key;
   StrStrMap* value;      // set on init/alloc
   BinTree * left;  // set to NULL on init
   BinTree * right; // set to NULL on init
};

BinTree *BinTree_alloc(StrStrMap *value);
StrStrMap* Bintree_delete(BinTree * node, int key, BinTree * parent);
void BinTree_free(BinTree *self);
void BinTree_deinit(BinTree *self);

