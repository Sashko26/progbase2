#pragma once
#include <vector>
#include <string>
#include <QMetaType>
using std::vector;
using std::string;
typedef struct metroStation
{
    int id;
    string nameOfStation;
    int hourOfOpening;
    string nameOfLine;

} metroStation;
Q_DECLARE_METATYPE(metroStation)


