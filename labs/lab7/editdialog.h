#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include <metroStation.h>

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit EditDialog(QWidget *parent = 0);
    void fillingLabebsInEditDialogDefaultData(metroStation st);
    bool checkLinesInEditDialog();
    metroStation getInstance();
    ~EditDialog();

private slots:
   // void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::EditDialog *ui;
};

#endif // EDITDIALOG_H
