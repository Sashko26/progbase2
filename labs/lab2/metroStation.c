#include "metroStation.h"
#include "csv.h"
void Csv_fromStructureArrayToTable(List *table, metroStation *testList, int len)
{

    for (int i = 0; i < len+1 ; i++)
    {
        if (i == 0)
        {
            List *pRow = List_alloc();
            Csv_addString(pRow, "Порядок букви в алфавіті");
            Csv_addString(pRow, "Назва Станції");
            Csv_addString(pRow, "година відкриття");
            Csv_addString(pRow, "Назва лінії");
            Csv_addRow(table, pRow);
        }
        else
        {
            struct metroStation *pSt = &testList[i - 1];
            List *pRow = List_alloc();
            Csv_addInt(pRow, pSt->orderLiteraInAlphabet);
            Csv_addString(pRow, pSt->nameOfStation);
            Csv_addInt(pRow, pSt->hourOfOpening);
            Csv_addString(pRow, pSt->nameOfLine);
            Csv_addRow(table, pRow);
        }
    }
}
metroStation *Csv_createArrayOfStructuresFromTable(List *Table)
{
    metroStation *arrayOfStation = malloc(sizeof(metroStation) * (List_size(Table) - 1));
    for (int i = 0; i < List_size(Table)-1; i++)
    {
        metroStation *pStation = &arrayOfStation[i];
        pStation->orderLiteraInAlphabet = atoi(((List *)Table->items[i + 1])->items[0]);
        pStation->nameOfStation = ((List *)Table->items[i + 1])->items[1];
        pStation->hourOfOpening = atoi(((List *)Table->items[i + 1])->items[2]);
        pStation->nameOfLine = ((List *)Table->items[i + 1])->items[3];
    }
    return arrayOfStation;
}
void Csv_createTablefromArrayStructuresWithOptionN(List *table, int len, metroStation *arrayOfstation, int N)
{
    for (int i = 0; i < len+1; i++)
    {
        if (i == 0)
        {
            List *pRow = List_alloc();

            Csv_addString(pRow, "Порядок букви в алфавіті");
            Csv_addString(pRow, "Назва Станції");
            Csv_addString(pRow, "година відкриття");
            Csv_addString(pRow, "Назва лінії");
            Csv_addRow(table, pRow);
        }

        else if ((&arrayOfstation[i - 1])->hourOfOpening < N)
        {
            struct metroStation *pSt = &arrayOfstation[i - 1];
            List *pRow = List_alloc();
            Csv_addInt(pRow, pSt->orderLiteraInAlphabet);
            Csv_addString(pRow, pSt->nameOfStation);
            Csv_addInt(pRow, pSt->hourOfOpening);
            Csv_addString(pRow, pSt->nameOfLine);
            Csv_addRow(table, pRow);
        }
    }
}
metroStation *createTestList()
{
    metroStation *testList = malloc(sizeof(metroStation) * 8); 
   
    testList[0].orderLiteraInAlphabet = 26;
    testList[1].orderLiteraInAlphabet = 20;
    testList[2].orderLiteraInAlphabet = 6;
    testList[3].orderLiteraInAlphabet = 23;
    testList[4].orderLiteraInAlphabet = 10;
    testList[5].orderLiteraInAlphabet = 29;
    testList[6].orderLiteraInAlphabet = 3;
    testList[7].orderLiteraInAlphabet = 4;

    testList[0].nameOfStation = "Хрещатик";
    testList[1].nameOfStation = "Політехнічний інститут";
    testList[2].nameOfStation = "Деміївська";
    testList[3].nameOfStation = "Теремки";
    testList[4].nameOfStation = "Золоті Ворота";
    testList[5].nameOfStation = "Шулявська";
    testList[6].nameOfStation = "Васильківська";
    testList[7].nameOfStation = "Голосіївська";

    testList[0].hourOfOpening = 4;
    testList[1].hourOfOpening = 8;
    testList[2].hourOfOpening = 12;
    testList[3].hourOfOpening = 18;
    testList[4].hourOfOpening = 21;
    testList[5].hourOfOpening = 15;
    testList[6].hourOfOpening = 24;
    testList[7].hourOfOpening = 13;

    testList[0].nameOfLine = "Святошинсько-Броварська лінія";
    testList[1].nameOfLine = "Святошинсько-Броварська лінія";
    testList[2].nameOfLine = "Оболонсько-Теремківська лінія";
    testList[3].nameOfLine = "Оболонсько-Теремківська лінія";
    testList[4].nameOfLine = "Сирецько-Печерська лінія";
    testList[5].nameOfLine = "Святошинсько-Броварська лінія";
    testList[6].nameOfLine = "Святошинсько-Броварська лінія";
    testList[7].nameOfLine = "Оболонсько-Теремківська лінія";
    return testList;

}
