#include "csv_storage.h"
#include "xml_storage.h"
#include "cui.h"
#include  "sqlite_storage.h"
#include <iostream>
using namespace std;

int main()
{
    bool workProgram=true;
    while(workProgram)
    {
        system("clear");
        cout<<"виберіть формат, з яким будете працювати:"<<endl;
        puts("1.xml");
        puts("2.csv");
        puts("3.sql");
        puts("4.exit");
        char ch;
        cin>>ch;
        if(ch!='1' && ch!='2' && ch!='3')
        {
              cout<<"введений некоректний символ! Спробуйте ще раз"<<endl;
              cin>>ch;
        }
        system("clear");
        if(ch=='1')
        {
            system("clear");
            XmlStorage xml_storage("../lab8/data/xml");
             Storage * storage_ptr =&xml_storage;
             storage_ptr->open();
             Cui cui(storage_ptr);
             cui.show();

        }
        else if(ch=='2')
        {
            system("clear");
            CsvStorage csv_storage("../lab8/data/csv");
            Storage * storage_ptr =&csv_storage;
             storage_ptr->open();
             Cui cui(storage_ptr);
             cui.show();
        }
        else if(ch=='3')
        {
            system("clear");
            SqliteStorage sql_storage("../lab8/data/sql");
            Storage * storage_ptr =&sql_storage;
            storage_ptr->open();
             Cui cui(storage_ptr);
             cui.show();
             storage_ptr->close();
        }
        else if(ch=='4')
        {
            system("clear");
            workProgram=false;
        }

    }
 return 0;
}

