#include "csv.h"
#include "list.h"
#include "Table.h"
typedef enum
{
    CsvReadState_Normal, 
    CsvReadState_Escape,
} CsvReadState;

void Csv_addRow(List *table, List *row)
{
    if (table->size == table->capacity)
    {
        List_realloc(table);
    }
    table->items[table->size] = row;
    table->size = table->size + 1;
}
void Csv_addInt(List *row, int value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    char stackBuf[100];
    sprintf(stackBuf, "%i", value);
    row->items[row->size] = createOnHeap(stackBuf);
    row->size = row->size + 1;
}
void Csv_addString(List *row, const char *value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    row->items[row->size] = createOnHeap(value);
    row->size = row->size + 1;
}
void Csv_addFloat(List *row, float value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    char stackBuf[100];
    sprintf(stackBuf, "%f", value);
    row->items[row->size] = createOnHeap(stackBuf);
    row->size = row->size + 1;
}
void Csv_fillTableFromString(List *csvTable, const char *csvString)
{

    char buf[100];
    int bufX = 0;
    char *p = (char *)csvString;
    List *pRow = List_alloc();
    while (1)
    {
        if (*p == ',')
        {
            buf[bufX] = '\0';
            Csv_addString(pRow, buf);
            bufX = 0;
        }
        else if (*p == '\n')
        {
            buf[bufX] = '\0';
            Csv_addString(pRow, buf);
            Csv_addRow(csvTable, pRow);
            pRow = NULL;
            pRow = List_alloc();
            bufX = 0;
        }
        else if (*p == '\0')
        {
            buf[bufX] = '\0';
            Csv_addRow(csvTable, pRow);
            pRow = NULL;
            bufX = 0;
        }
        else
        {
            buf[bufX] = *p;
            bufX += 1;
        }

        if (*p == '\0')
        {
            break;
        }
        p += 1;
    }
    List_clear(csvTable->items[csvTable->size - 1]);
    List_free(csvTable->items[csvTable->size - 1]);
    csvTable->size = csvTable->size-1;
}

//аналог fillStringFromTable
char* Csv_fillStringFromTable(List *Table)
{
    char *string;
    string = Csv_fillStringFromList((Table)->items[0]);
    for (int i = 1; i < List_size(Table); i++)
    {
        char *buf = Csv_fillStringFromList((Table)->items[i]);
        string = strcat(string, buf);
        free(buf);
    }
    return string;
}

char *Csv_fillStringFromList(List *Row)
{
    char *string;
    char *stroka = malloc(sizeof(char) * 1000);
    stroka[0] = '\0';
    for (int i = 0; i < List_size(Row) - 1; i++)
    {
        string = List_get(Row, i);
        strcat(stroka, string);
        strcat(stroka, ",");
    }
    string = List_get(Row, List_size(Row) - 1);
    strcat(stroka, string);
    strcat(stroka, "\n");
    strcat(stroka, "\0");
    return stroka;
}


