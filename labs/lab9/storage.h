#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "optional.h"
#include "metroStation.h"
#include "metroLine.h"
#include "user.h"

using namespace std;

class Storage
{
 public:
    //virtual ~Storage(){}
   virtual bool open() = 0;
   virtual bool close() = 0;
   // metroStations
   virtual vector<metroStation> getAllmetroStations(void) = 0;
   virtual optional<metroStation> getmetroStationById(int metroStation_id) = 0;
   virtual bool updatemetroStation(const metroStation &metroStation) = 0;
   virtual bool removemetroStation(int metroStation_id) = 0;
   virtual int insertmetroStation(const metroStation &metroStation) = 0;
   // metroLine
   virtual vector<metroLine> getAllmetroLines(void) = 0;
   virtual optional<metroLine> getmetroLineById(int metroLine_id) = 0;
   virtual bool updatemetroLine(const metroLine &metroLine) = 0;
   virtual bool removemetroLine(int metroLine_id) = 0;
   virtual int insertmetroLine(const metroLine &metroLine) = 0;

       virtual vector<metroStation> getAllUserMetroStations(int user_id) = 0;

    // users
     virtual optional<User> getUserAuth(QString & username, QString & password) = 0;
     // links
     virtual vector<metroLine> getAllLinesOfstation(int station_id) = 0;
     virtual bool insertLineinStation(int station_id, int line_id) = 0;
     virtual bool removeLineinStation(int station_id, int line_id)= 0;

};
