#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <xml_storage.h>
#include <QDir>
#include <QString>
#include <vector>
#include "sqlite_storage.h"

#include "adddialog.h"
#include "editdialog.h"
#include "authentication.h"
void MainWindow::clear()
{
    ui->editButton->setEnabled(false);
    ui->removeButton->setEnabled(false);
    ui->label_StationName->clear();
    ui->spinBox->clear();
    ui->label_LineName->clear();
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
     ui->setupUi(this);
     authentication * auth= new authentication(this);

     bool inCorrectData=true;
     bool label_for_incorrectData=false;
     while(inCorrectData)
     {
            int status;
                    if(label_for_incorrectData)
                    {
                        status=auth->exec();
                        auth->LabelInCorrect();
                    }
                    else
                    {
                        status=auth->exec();
                    }

            if(status==1)
            {
                QString login = auth->getLogin();
                QString password =auth->getPassword();

                    qDebug()<<login;
                    qDebug()<<password;
                 if(this->storage_->getUserAuth(login,password)!=nullopt)
                 {
                     inCorrectData=false;
                     connect(ui->actionNew_Storage, &QAction::triggered, this, &MainWindow::New_Storage);
                     connect(ui->actionExit, &QAction::triggered, this, &MainWindow::beforeClose);
                     connect(ui->actionOpen_Storage, &QAction::triggered, this, &MainWindow::Open_Storage);
                     ui->addButton->setEnabled(false);
                     ui->editButton->setEnabled(false);
                     ui->removeButton->setEnabled(false);
                     ui->spinBox->setEnabled(false);
                     this->storage_=nullptr;
                 }
                 else
                 {
                     label_for_incorrectData=true;
                 }

            }
            else
                {
                    auth->close();
                    inCorrectData=false;
                        exit(0);
                }
     }













}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}


void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this, "On exit", "Are you sure?",
                                QMessageBox::StandardButton::Abort | QMessageBox::StandardButton::Apply);
    if(reply==QMessageBox::StandardButton::Abort)
    {
        this->close();
    }
    else
    {
        qDebug() << "Do not exit";
    }

}
//встановлення нової сутності в listWidget/
void MainWindow::setData(metroStation metrostation)
{
    QVariant qVariant;
        qVariant.setValue(metrostation);

        QListWidgetItem *qMetroStationListItem = new QListWidgetItem();
        QString q_name_of_station=QString::fromStdString(metrostation.nameOfStation);
        qMetroStationListItem->setText(q_name_of_station);
        qMetroStationListItem->setData(Qt::UserRole, qVariant);
        ui->listWidget->addItem(qMetroStationListItem);
 }


void MainWindow::Open_Storage()
{
    if(this->storage_!=nullptr)
    {
        delete this->storage_;
    }

     QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                  "../",
                QFileDialog::ShowDirsOnly
           | QFileDialog::DontResolveSymlinks);
    qDebug() << dir;
    string std_dir=dir.toStdString();
    Storage * storage=new SqliteStorage(std_dir);
    this->storage_=storage;
    storage->open();
    vector<metroStation> stations=this->storage_->getAllmetroStations();
    if(this->ui->listWidget->count()!=0)
//
    {
        ui->listWidget->clear();

    }

     for(int i=0;i<static_cast<int>(stations.size());i++)
        {
            metroStation metrostation=stations[i];
            this->setData(metrostation);
        }
        ui->addButton->setEnabled(true);

}
void MainWindow::New_Storage()
{   if(this->storage_!=nullptr)
    {
        delete this->storage_;
    }
     QFileDialog dialog(this);
      dialog.setFileMode(QFileDialog::Directory);
      QString current_dir = QDir::currentPath();
      QString default_name = "new_storage";
      QString folder_path = dialog.getSaveFileName(
          this,
          "Select New Storage Folder",
          current_dir + "/" + default_name,
          "Folders"); qDebug() << folder_path;
        QDir().mkdir(folder_path);
        QString fname = folder_path+"/metroStation.xml";
        QFile file(fname);
        if (file.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            file.close();
        }


        string std_folder_path=folder_path.toStdString();
        Storage * storage=new SqliteStorage(std_folder_path);
        this->storage_=storage;
        storage->open();
        vector<metroStation> stations=this->storage_->getAllmetroStations();
        if(this->ui->listWidget->count()!=0)
        {
            ui->listWidget->clear();
        }
        for(int i=0;i<static_cast<int>(stations.size());i++)
           {
               metroStation metrostation=stations[i];
               this->setData(metrostation);
           }
           ui->addButton->setEnabled(true);



           fname = folder_path+"/id.txt";
        QFile fileId(fname);
        if (fileId.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            fileId.write("1");
            fileId.close();
        }




}



void MainWindow::on_listWidget_itemClicked(QListWidgetItem *items)
{

    if(items->isSelected()==false)
    {
        QList<QListWidgetItem *> list_items =ui->listWidget->selectedItems();
        if(list_items.size()==0)
        {
             this->clear();
        }
        else
        {
            ui->label_StationName->clear();


            ui->label_openTime->clear();



            ui->spinBox->clear();


            ui->label_StationName->clear();
            QVariant var = list_items[list_items.size()-1]->data(Qt::UserRole);
            metroStation st = var.value<metroStation>();
            QString q_st_name=QString::fromStdString(st.nameOfStation);
            ui->label_StationName->setText("Station name: " + q_st_name);


        //    QString q_st_openingtime=QString::number(st.hourOfOpening);


            int q_st_openingtime=st.hourOfOpening;
            ui->spinBox->setValue(q_st_openingtime);
            ui->label_openTime->setText("Opening time: ");




          //  ui->label_OpeningTime->setText("Opening time: " +  q_st_openingtime);


            QString q_st_linename=QString::fromStdString(st.nameOfLine);
            ui->label_LineName->setText("Line name: " + q_st_linename);
        }

    }

    else
    {
          QVariant var = items->data(Qt::UserRole);
          metroStation st = var.value<metroStation>();
          QString q_st_name=QString::fromStdString(st.nameOfStation);
          ui->label_StationName->setText("Station name: " + q_st_name);




          int q_st_openingtime=st.hourOfOpening;
          ui->spinBox->setValue(q_st_openingtime);
          ui->label_openTime->setText("Opening time: ");



          QString q_st_linename=QString::fromStdString(st.nameOfLine);
          ui->label_LineName->setText("Line name: " + q_st_linename);

          ui->editButton->setEnabled(true);
          ui->removeButton->setEnabled(true);

    }

}

void MainWindow::on_addButton_clicked()
{
    AddDialog addDialog(this);
    int status=addDialog.exec();
       if(status==1)
     {
           if(addDialog.checkLinesInAddDialog()==true)
           {
               metroStation station=addDialog.data(); //зчитали дані з полів
               this->storage_->insertmetroStation(station); //вставили нову сутність  у вектор у стореджі і добавили її у файл
               this->setData(station); // добавили сутність у ЛістВіджет
           }

     }
qDebug() << status;
}
void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> items =ui->listWidget->selectedItems();
    if(items.count() == 0)
    {
        qDebug()<< "nothing selected";
    }
    else
    {
        foreach (QListWidgetItem * selectedItem, items)
        {
            int row_index =ui->listWidget->row(selectedItem);
            ui->listWidget->takeItem(row_index);
            QVariant var = selectedItem->data(Qt::UserRole);
            metroStation st = var.value<metroStation>();
            int id =st.id;
            this->storage_->removemetroStation(id);
            delete selectedItem;
        }
    }
    ui->label_StationName->clear();


    ui->label_openTime->clear();
    ui->spinBox->clear();


    ui->label_LineName->clear();
    if(ui->listWidget->selectedItems().count()==0)
    {
        ui->editButton->setEnabled(false);
        ui->removeButton->setEnabled(false);
        ui->spinBox->setEnabled(false);

    }
}

void MainWindow::on_editButton_clicked()
{

    EditDialog editDialog(this);

    QListWidgetItem* stationInWidgetList=ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1];
    QVariant var=stationInWidgetList->data(Qt::UserRole);
    metroStation st = var.value<metroStation>();
    editDialog.fillingLabebsInEditDialogDefaultData(st);

    int status=editDialog.exec();

    if(status==1)
    {
        if(editDialog.checkLinesInEditDialog()==true)
        {  qDebug()<<"hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello ";
            metroStation station;
            station=editDialog.getInstance();

            this->storage_->updatemetroStation(station);

            qDebug()<<QString::fromStdString(station.nameOfStation);
            qDebug()<<QString::fromStdString(station.nameOfLine);
            qDebug()<<station.hourOfOpening;

            this->ui->label_StationName->setText("Station name: "+QString::fromStdString(station.nameOfStation));
            this->ui->label_LineName->setText("Line time: "+QString::fromStdString(station.nameOfLine));


            this->ui->label_openTime->setText("Opening time: ");
            this->ui->spinBox->setValue(station.hourOfOpening);


            ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1]->setText(QString::fromStdString(station.nameOfStation));


            QVariant qVariant;
                qVariant.setValue(station);
            ui->listWidget->selectedItems()[ui->listWidget->selectedItems().size()-1]->setData(Qt::UserRole, qVariant);
        }



    }
    else
    {
        qDebug()<<"changes opinion";
    }
}
