#include "StrStrMap.h"
#include "BSTree.h"
#include "BinTree.h"


/* static StrStrMap *BinTree_delete(BinTree *node, int key, BinTree *parent); */
static BinTree *searchMin(BinTree *node);

void deleteElementsFromBSTreeWithoptionN(List *listOfMap, BSTree *tree, int n)
{
    for (int i = 0; i < List_size(listOfMap); i++)
    {
        if (atoi(StrStrMap_get(listOfMap->items[i], "hourOfOpening")) >= n)
        {
            int key = atoi(StrStrMap_get(listOfMap->items[i], "orderLiteraInAlphabet"));
            BSTree_delete(tree, key);
        }
    }
}

BSTree *createBSTreeFromlistMap(List *listOfMap)
{
    BSTree *Tree = BSTree_alloc();
    for (int i = 0; i < List_size(listOfMap); i++)
    {
        BSTree_insert(Tree, listOfMap->items[i]);
    }
    return Tree;
}
static void printValueOnLevel(BinTree *node, char pos, int depth)
{
    for (int i = 0; i < depth; i++)
    {
        printf("....");
    }
    printf("%c: ", pos);

    if (node == NULL)
    {
        printf("(null)\n");
    }
    else
    {
        printf("[%i]`%s`\n", node->key, StrStrMap_get(node->value, "nameOfStation"));
    }
}

void print(BinTree *node, char pos, int depth)
{
    bool isNotNull = (node != NULL) && (node->left != NULL || node->right != NULL);
    if (isNotNull)
        print(node->right, 'R', depth + 1);
    printValueOnLevel(node, pos, depth);
    if (isNotNull)
        print(node->left, 'L', depth + 1);
}

BinTree *BStree_getRoot(BSTree *tree)
{
    BinTree *root = tree->root;
    return root;
}
void printTree(BinTree *root)
{
    print(root, '+', 0);
}

//group for delete

static void modifyTreeOnDelete(BinTree *node, BinTree *parent);
static StrStrMap *delete (BinTree *node, int key, BinTree *parent)
{
    if (node == NULL)
    {
        fprintf(stderr, "`%i` not found\n", key);
        abort();
    }
    if (key < node->key)
    {
        return delete (node->left, key, node);
    }
    else if (key > node->key)
    {
        return delete (node->right, key, node);
    }
    else
    {
        modifyTreeOnDelete(node, parent);
        StrStrMap *old = node->value;
        BinTree_free(node);
        return old;
    }
}
StrStrMap *BSTree_delete(BSTree *self, int key)
{
    BinTree fakeRoot;
    fakeRoot.left = self->root;
    StrStrMap *old = delete (self->root, key, &fakeRoot);
    self->root = fakeRoot.left;

    return old;
}
static void modifyTreeOnDelete(BinTree *node, BinTree *parent)
{
    BinTree *replacementNode = NULL;
    if (node->left == NULL && node->right == NULL)
    {
        replacementNode = NULL;
    }
    else if (node->left == NULL || node->right == NULL)
    {
        BinTree *child = (node->left != NULL) ? node->left : node->right;
        replacementNode = child;
    }
    else
    {
        BinTree *minNode = searchMin(node->right);
        int minKey = minNode->key;
        StrStrMap *deletedValue = delete (node->right, minKey, node);
        BinTree *newMin = BinTree_alloc(deletedValue);
        newMin->left = node->left;
        newMin->right = node->right;
        replacementNode = newMin;
    }
    if (parent->left == node)
    {
        parent->left = replacementNode;
    }
    else
    {
        parent->right = replacementNode;
    }
}

//group for initing and deiniting and free

BSTree *BSTree_alloc()
{
    BSTree *Tree = malloc(sizeof(BSTree));
    Tree->root = NULL;
    return Tree;
}

void BSTree_free(BSTree *self)
{
    clearTree(self->root);
    free(self);
}
void clearTree(BinTree *node)
{
    if (node == NULL)
        return;
    clearTree(node->left);
    clearTree(node->right);
    free(node);
}

//group insert
static void insert(BinTree *node, BinTree *newNode)
{
    if (newNode->key == node->key)
    {
        fprintf(stderr, "`%i` Already exists\n", newNode->key);
        abort();
    }
    if (newNode->key < node->key)
    {
        if (node->left == NULL)
        {
            node->left = newNode;
        }
        else
        {
            insert(node->left, newNode);
        }
    }
    else if (newNode->key > node->key)
    {
        if (node->right == NULL)
        {
            node->right = newNode;
        }
        else
        {
            insert(node->right, newNode);
        }
    }
}

void BSTree_insert(BSTree *self, StrStrMap *value)
{
    BinTree *newNode = BinTree_alloc(value);
    if (self->root == NULL)
    {
        self->root = newNode;
    }
    else
    {
        insert(self->root, newNode);
    }
}
//// group lookup
static bool lookup(BinTree *node, int key)
{
    if (node == NULL)
        return false;
    if (node->key == key)
        return true;
    if (key < node->key)
        return lookup(node->left, key);
    if (key > node->key)
        return lookup(node->right, key);
    return NULL;
}

bool BSTree_lookup(BSTree *self, int key)
{
    return lookup(self->root, key);
}

///group search
static StrStrMap *search(BinTree *node, int key)
{
    if (node == NULL)
        return NULL;
    if (node->key == key)
        return node->value;
    if (key < node->key)
        return search(node->left, key);
    if (key > node->key)
        return search(node->right, key);
    return NULL;
} ////

StrStrMap *BSTree_search(BSTree *self, int key)
{
    return search(self->root, key);
}
 BinTree *searchMin(BinTree *node)
{
    if (node == NULL)
        return NULL;
    if (node->left == NULL)
        return node;
    return searchMin(node->left);
}
