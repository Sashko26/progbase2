#pragma once
#include <iostream>
#include <fstream>
//#include <sstream>
#include "optional.h"
#include "metroStation.h"


using namespace std;

class Storage
{
 public:
   virtual bool load() = 0;
   virtual bool save() = 0;
   // metroStations
   virtual vector<metroStation> getAllmetroStations(void) = 0;
   virtual optional<metroStation> getmetroStationById(int metroStation_id) = 0;
   virtual bool updatemetroStation(const metroStation &metroStation) = 0;
   virtual bool removemetroStation(int metroStation_id) = 0;
   virtual int insertmetroStation(const metroStation &metroStation) = 0;
};
