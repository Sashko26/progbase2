#pragma once
#include "list.h"
typedef struct metroStation
{
    float orderLiteraInAlphabet;
    char *nameOfStation;
    int hourOfOpening;
    char *nameOfLine;

} metroStation;
metroStation* Csv_createArrayOfStructuresFromTable(List *Table);
void Csv_fromStructureArrayToTable(List *table, metroStation *testList, int len);
metroStation *Csv_createArrayOfStructures(List *Table);
void Csv_fromStructureArrayToTable(List *table, metroStation *testList, int len);
void Csv_createTablefromArrayStructuresWithOptionN(List *table, int len, metroStation *arrayOfstation, int N);
metroStation *createTestList();