#pragma once
#include <cstdlib>
#include <iostream>
#include <math.h>
using namespace std;
class Deque
{
private:
  void enlarge(int newCapacity)
  {
    float *newMem = static_cast<float *>(realloc(this->_array, sizeof(float) * newCapacity));
    if (newMem == nullptr)
    {
      free(this->_array);
      cout << "reallocation error" << endl;
      abort();
    }
    this->_array = newMem;
    this->_capacity = newCapacity;
  }
  float *_array;
  int _length;
  int _capacity;
  int _front;

public:
  Deque();
  ~Deque();
  void pushByFront(float value);
  float popByFront();
  void pushByRear(float value);
  float popByRear();
  float frontPeek();
  float rearPeek();
  int size();
  float getValueByindex(int index);

  int indexOfFrontPeek();
  int indexOfRearPeek();
  void print();
};
