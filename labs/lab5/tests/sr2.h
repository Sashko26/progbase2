
#pragma once

#include <string>
#include <vector>

using namespace std;

namespace Sr2
{
    enum NumberType
    {
        Integer,    //
        Double,  //
    };

    struct Number
    {
        string number;
        NumberType type;

        Number();
        Number(const string & number);
        Number(const string & number, NumberType type);
    };

    vector<Number> getNumbers(const string & text);
}


