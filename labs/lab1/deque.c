// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>  // Для друку в термінал
#include <math.h>   // Для математичних функцій
#include <stdlib.h> // Деякі додаткові функції

#include "deque.h"


float Deque_getValueByindex(Deque *self, int index);
void Deque_realloc(Deque *self, int newCapacity);
int Deque_indexOfFrontPeek(Deque *self);
int Deque_indexOfRearPeek(Deque *self);

// ініціалізація деки
void Deque_init(Deque *self)
{
    self->length = 0;
    self->front = -1;
    self->capacity = 4;
    self->array = malloc(sizeof(float) * self->capacity);
    if (self->array == NULL)
    {
        fprintf(stderr, "Allocation error");
        abort();
    }
}
// функція для отримання значення передньої вершини
float frontPeek(Deque *self)
{
    float val = self->array[self->front];
    return val;
}
// функція для отримання значення задньої вершини
float rearPeek(Deque *self)
{
    float val = self->array[self->front + self->length - 1];
    return val;
}
int Deque_size(Deque *self)
{
    int val=self->length;
    return val;
}
void Deque_pushByFront(Deque *self, float value)
{
    if (self->front == 0)
    {
        int newCapacity = self->capacity * 2;
        Deque_realloc(self, newCapacity);
        for (int i = 0; i < self->length; i++)
        {
            self->array[self->front + i + self->length] = self->array[self->front + i];
        }
        self->array[self->front + self->length - 1] = value;
        self->front = self->front + self->length - 1;
        self->length = self->length + 1;
    }
    else if (self->front == -1)
    {
        self->front = 0;
        self->array[self->front] = value;
        self->length = self->length + 1;
    }
    else
    {
        self->front = self->front - 1;
        self->array[self->front] = value;
        self->length = self->length + 1;
    }
}
int Deque_indexOfFrontPeek(Deque *self)
{
    int val=self->front;
    return val;
}
int Deque_indexOfRearPeek(Deque *self)
{
    int val=self->front+self->length-1;
    return val;
}

//видалення елементу деки з початку
float Deque_popByFront(Deque *self)
{
    if (self->length == 0)
    {
        fprintf(stderr, "stack is underflow");
        abort();
    }
    else if (self->length == 1)
    {
        int val = self->array[self->front];
        self->length = self->length - 1;
        self->front = -1;
        return val;
    }
    else
    {
        self->front = self->front + 1;
        self->length = self->length - 1;
        return self->array[self->front - 1];
    }
}
float Deque_getValueByindex(Deque *self, int index)
{
    float val=self->array[self->front+index];
    return val;
}
//видалення елементу деки з кінця
float Deque_popByRear(Deque *self)
{
    if (self->length == 0)
    {
        fprintf(stderr, "stack is underflow");
        abort();
    }
    float val = self->array[self->front + self->length -1 ];
    self->length = self->length - 1;
    return val;
}
// вставлення елементу деки в кінець
void Deque_pushByRear(Deque *self, float value)
{
    if (self->front + self->length == self->capacity)
    {
        int newCapacity = self->capacity * 2;
        Deque_realloc(self, newCapacity);
        self->length = self->length + 1;
        self->array[self->front + self->length - 1] = value;
        self->capacity = newCapacity;
    }
    else if (self->front == -1)
    {
        self->front = 0;
        self->length = 1;
        self->array[self->front] = value;
    }
    else
    {
        self->length = self->length + 1;
        self->array[self->front + self->length - 1] = value;
    }
}
// перевиділення динамічної пам'яті
void Deque_realloc(Deque *self, int newCapacity)
{
    float *newMem = realloc(self->array, sizeof(float) * newCapacity);
    if (newMem == NULL)
    {
        free(self->array);
        fprintf(stderr, "reallocation error");
        abort();
    }
    self->array = newMem;
    self->capacity = newCapacity;
}
// звільнення динамічної пам'яті від деки
void Deque_deinit(Deque *self)
{
    free(self->array);
}
void Deque_print(Deque *self)
{
    if (self->front == -1)
    {
        fprintf(stderr, "Deque is empty!");
    }
    else
    {
        int beginOfprint = self->front;
        int endOfprint = self->front + self->length;
        for (int i = beginOfprint; i < endOfprint; i++)
        {
            printf("|%f| ", self->array[i]);
        }
        puts("");
    }
}
