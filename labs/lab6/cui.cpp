#include "cui.h"
void Cui::metroStationUpdateMenu(int id)
{
    optional<metroStation> opt_value = this->storage_->getmetroStationById(id);
    if (opt_value)
    {
        bool updateInformationAboutStation = true;
        while (updateInformationAboutStation)
        {
            char ch;
            system("clear");

            cout << "1.Змінити назву станції" << endl;
            cout << "2.Змінити назву лінії" << endl;
            cout << "3.Змінити час відкриття станції" << endl;
            cout << "4.Вийти в попереднє мемю" << endl;

            cin >> ch;
            cin.ignore();
            while (ch != '1' && ch != '2' && ch != '3' && ch != '4')
            {
                puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
                cin >> ch;
            }
            if (ch == '1')
            {
                puts("Введіть нову назву:");
                string str;
                //bool enterNum = true;
                getline(cin, str);
                metroStation station = opt_value.value();
                station.nameOfStation = str;
                metroStation &ref = station;
                this->storage_->updatemetroStation(ref, ch);
            }
            else if (ch == '2')
            {
                puts("Введіть назву лінії:");
                string str;

                                                 //bool enterNum = true;
                getline(cin, str);
                metroStation station = opt_value.value();
                station.nameOfLine = str;
                metroStation &ref = station;
                this->storage_->updatemetroStation(ref, ch);
            }
            else if (ch == '3')
            {
                puts("Введіть новий час відкриття:");
                string str;
                bool enterNum = true;
                while (enterNum)
                {
                    getline(cin, str);
                    char stroka[100];
                    strcpy(stroka, str.c_str());
                    enterNum = false;
                    for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                    {
                        if (!isdigit(stroka[i]))
                        {
                            enterNum = true;
                            cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                            break;
                        }
                    }
                }
                metroStation station = opt_value.value();
                station.hourOfOpening = stoi(str);
                metroStation &ref = station;
                this->storage_->updatemetroStation(ref, ch);
            }
            else if (ch == '4')
            {
                updateInformationAboutStation = false;
            }
        }
    }
    else
    {
        system("clear");
        cout << "Cтанції із заданим ID у базі не існує" << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
}

void Cui::printStringsTableLittle(CsvTable &table)
{
    for (int i = 1; i < static_cast<int>(table.size()); i++)
    {
        CsvRow row = table[i];
        printStringsLittle(row);
    }
}

void Cui::printStringsLittle(CsvRow &row)
{
    for (int i = 0; i < 2; i++)
    {
        string str = row[i];
        if (i == 0)
        {
            cout << str << "-";
        }
        else
        {
            cout << str;
        }
    }
    printf("\n");
}
void Cui::printStrings(CsvRow &row)
{
    for (int i = 0; i < static_cast<int>(row.size()); i++)
    {
        string str = row[i];
        cout << str << ",";
    }
    printf("\n");
}

void Cui::metroStationsMainMenu()
{
    system("clear");
    CsvTable table;
    CsvRow row;
    row.push_back("id");
    row.push_back("Назва станції метро");
    row.push_back("Година відкриття");
    row.push_back("Назва лінії");
    table.push_back(row);
    vector<metroStation> metroStations = this->storage_->getAllmetroStations();
    for (int i = 0; i < static_cast<int>(metroStations.size()); i++)
    {
        CsvRow row;
        row.push_back(to_string(metroStations[i].id));
        row.push_back(metroStations[i].nameOfStation);
        row.push_back(to_string(metroStations[i].hourOfOpening));
        row.push_back(metroStations[i].nameOfLine);
        table.push_back(row);
    }
    CsvTable &ref = table;
    this->printStringsTableLittle(ref);
    cout << "1.Переглянути детальну інформацію про обрану сутність" << endl;
    cout << "2.Змінити поле обраної сутності (окрім ідентифікатора)" << endl;
    cout << "3.Видалити обрану сутність" << endl;
    cout << "4.Cтворити нову станцію" << endl;
    cout << "5.Вихід" << endl;
}
void Cui::metroStationMenu(int entity_id)
{
    optional<metroStation> station_opt = this->storage_->getmetroStationById(entity_id);
    system("clear");
    if (station_opt)
    {
        metroStation station = station_opt.value();
        cout << "ID:" << station.id << endl;
        cout << "Назва станції:" << station.nameOfStation << endl;
        cout << "Час відкриття:" << station.hourOfOpening << endl;
        cout << "Назва лінії:" << station.nameOfLine << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
    else
    {
        cout << "Cтанції із заданим ID немає у базі даних." << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        if (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
}
void Cui::show()
{
    {
        bool startMenu = true;
        while (startMenu)
        {
            vector<metroStation> st=this->storage_->getAllmetroStations();
            for(int i=0;i<static_cast<int>(st.size());i++)
            {
                cout<<st.at(i).id <<" "<<st.at(i).nameOfStation<< st.at(i).hourOfOpening << st.at(i).nameOfLine<<endl;

            }
            storage_->removemetroStation(2);
            puts("");
            for(int i=0;i<static_cast<int>(st.size());i++)
            {
                cout<<st.at(i).id <<" "<<st.at(i).nameOfStation<< st.at(i).hourOfOpening << st.at(i).nameOfLine<<endl;

            }
             system("clear");
            puts("1.Станція метро");
            puts("2.Лінія метро");
            puts("3.Вихід");
            char ch;

            cin >> ch;
            cin.ignore();
            while (ch != '1' && ch != '2' && ch != '3')
            {
                puts("Введено некоректний символ, спробуйте будь ласка ще раз:");

                cin >> ch;
                cin.ignore();
            }
            if (ch == '1')
            {
                bool mainMenuOfMetroStation = true;
                while (mainMenuOfMetroStation)
                {
                    this->metroStationsMainMenu();
                    char ch;

                    cin >> ch;
                    cin.ignore();
                    while (ch != '1' && ch != '2' && ch != '3' && ch != '4' && ch != '5')
                    {
                        puts("Введено некоректний символ, спробуйте будь ласка ще раз:");

                        cin >> ch;
                        cin.ignore();
                    }
                    switch (ch)
                    {
                    case '1':
                    {
                        system("clear");
                        cout << "Введіть ID бажаної сутності" << endl;
                        string str;
                        bool enterNum = true;
                        while (enterNum)
                        {
                            cin >> str;
                            char stroka[100];
                            strcpy(stroka, str.c_str());
                            enterNum = false;
                            for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                            {
                                if (!isdigit(stroka[i]))
                                {
                                    enterNum = true;
                                    cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                                    break;
                                }
                            }
                        }
                        int id = stoi(str);
                        this->metroStationMenu(id);
                    }
                    break;
                    case '2':
                    {
                        system("clear");
                        puts("Введіть ID:");
                        string str;
                        bool enterNum = true;
                        while (enterNum)
                        {
                            cin >> str;
                            char stroka[100];
                            strcpy(stroka, str.c_str());
                            enterNum = false;
                            for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                            {
                                if (!isdigit(stroka[i]))
                                {
                                    enterNum = true;
                                    cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                                    break;
                                }
                            }
                        }
                        cout << str << "| - |" << endl;
                        int id = stoi(str);
                        this->metroStationUpdateMenu(id);
                    }
                    break;
                    case '3':
                    {
                        system("clear");
                        puts("Введіть ID:");
                        string str;
                        bool enterNum = true;
                        while (enterNum)
                        {
                            cin >> str;
                            char stroka[100];
                            strcpy(stroka, str.c_str());
                            enterNum = false;
                            for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                            {
                                if (!isdigit(stroka[i]))
                                {
                                    enterNum = true;
                                    cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                                    break;
                                }
                            }
                        }
                        int id = stoi(str);
                        this->metroStationDeleteMenu(id);
                    }
                    break;
                    case '4':
                    {
                        this->metroStationCreateMenu();
                    }
                    break;
                    case '5':
                    {
                        mainMenuOfMetroStation = false;
                    }
                    break;
                    }
                }
            }
            else if (ch == '2')
            {
                bool mainMenuOfMetroStation = true;
                while (mainMenuOfMetroStation)
                {
                    this->metroLinesMainMenu();
                    char ch;

                    cin >> ch;
                    cin.ignore();
                    while (ch != '1' && ch != '2' && ch != '3' && ch != '4' && ch != '5')
                    {
                        puts("Введено некоректний символ, спробуйте будь ласка ще раз:");

                        cin >> ch;
                        cin.ignore();
                    }
                    switch (ch)
                    {
                    case '1':
                    {
                        system("clear");
                        cout << "Введіть ID бажаної сутності" << endl;
                        string str;

                        bool enterNum = true;
                        while (enterNum)
                        {
                            cin >> str;
                            char stroka[100];
                            strcpy(stroka, str.c_str());
                            enterNum = false;
                            for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                            {
                                if (!isdigit(stroka[i]))
                                {
                                    enterNum = true;
                                    cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                                    break;
                                }
                            }
                        }
                        int id = stoi(str);
                        this->metroLineMenu(id);
                    }
                    break;
                    case '2':
                    {
                        system("clear");
                        puts("Введіть ID:");
                        string str;

                        bool enterNum = true;
                        while (enterNum)
                        {
                            cin >> str;
                            char stroka[100];
                            strcpy(stroka, str.c_str());
                            enterNum = false;
                            for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                            {
                                if (!isdigit(stroka[i]))
                                {
                                    enterNum = true;
                                    cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                                    break;
                                }
                            }
                        }

                        int id = stoi(str);
                        this->metroLineUpdateMenu(id);
                    }
                    break;
                    case '3':
                    {
                      system("clear");
                        puts("Введіть ID:");
                        string str;
                        cin >> str;
                        int id = stoi(str);
                        this->metroLineDeleteMenu(id);
                    }
                    break;
                    case '4':
                    {
                        this->metroLineCreateMenu();
                    }
                    break;
                    case '5':
                    {
                        mainMenuOfMetroStation = false;
                    }
                    break;
                    }
                }
            }
            else if (ch == '3')
            {
                startMenu = false;
            }
        }
    }
}

void Cui::metroStationDeleteMenu(int id)
{
    optional<metroStation> opt_value = this->storage_->getmetroStationById(id);
    if (opt_value)
    {
        this->storage_->removemetroStation(id);
        cout << "Станцію видалено" << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");

            cin >> ch;
            cin.ignore();
        }
    }
    else
    {
        system("clear");
        cout << "Cтанції із заданим ID у базі не існує" << endl;
        cout << "1.Вихід" << endl;
        char ch;

        cin >> ch;
        cin.ignore();
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
}

///line interface
void Cui::metroLinesMainMenu()
{
    {
        {
            system("clear");
            CsvTable table;
            CsvRow row;
            row.push_back("id");
            row.push_back("Назва лінії");
            row.push_back("Кількість станцій");
            row.push_back("Колір ліній");
            table.push_back(row);
            vector<metroLine> metroLines = this->storage_->getAllmetroLines();
            for (int i = 0; i < static_cast<int>(metroLines.size()); i++)
            {
                CsvRow row;
                row.push_back(to_string(metroLines[i].id));
                row.push_back(metroLines[i].nameOfline);
                row.push_back(to_string(metroLines[i].amountOfstation));
                row.push_back(metroLines[i].colourOfLine);
                table.push_back(row);
            }
            CsvTable &ref = table;
            this->printStringsTableLittle(ref);
            cout << "1.Переглянути детальну інформацію про обрану сутність" << endl;
            cout << "2.Змінити поле обраної сутності (окрім ідентифікатора)" << endl;
            cout << "3.Видалити обрану сутність" << endl;
            cout << "4.Cтворити нову лінію метро" << endl;
            cout << "5.Вихід" << endl;
        }
    }
}

void Cui::metroLineMenu(int entity_id)
{
    {
        optional<metroLine> line_opt = this->storage_->getmetroLineById(entity_id);
        system("clear");
        if (line_opt)
        {
            metroLine line = line_opt.value();
            cout << "ID:" << line.id << endl;
            cout << "Назва лінії:" << line.nameOfline << endl;
            cout << "Кількість станцій:" << line.amountOfstation << endl;
            cout << "Колір лінії:" << line.colourOfLine << endl;
            cout << "1.Вихід" << endl;
            char ch;
            cin >> ch;
            while (ch != '1')
            {
                puts("Введено некоректний символ, спробуйте будь ласка ще раз:");

                cin >> ch;
                cin.ignore();
            }
        }
        else
        {
            cout << "Cтанції із заданим ID немає у базі даних." << endl;
            cout << "1.Вихід" << endl;
            char ch;

            cin >> ch;
            cin.ignore();
            if (ch != '1')
            {
                puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
                cin >> ch;
            }
        }
    }
}
void Cui::metroLineUpdateMenu(int id)
{
    optional<metroLine> opt_value = this->storage_->getmetroLineById(id);
    if (opt_value)
    {
        bool updateInformationAboutLine = true;
        while (updateInformationAboutLine)
        {
            char ch;
            system("clear");
            cout << "1.Змінити назву лінії" << endl;
            cout << "2.Змінити кількість станцій" << endl;
            cout << "3.Змінити колір лінії" << endl;
            cout << "4.Вийти в попереднє мемю" << endl;

            cin >> ch;
            while (ch != '1' && ch != '2' && ch != '3' && ch != '4')
            {
                puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
                cin >> ch;
                /* cin.ignore(); */
            }
            if (ch == '1')
            {
                puts("Введіть нову назву:");
                string str;
                getline(cin, str);
                metroLine line = opt_value.value();
                line.nameOfline = str;
                this->storage_->updatemetroLine(line, ch);
            }
            else if (ch == '2')
            {
                puts("Введіть нову кількість:");
                string str;

                bool enterNum = true;
                while (enterNum)
                {

                    cin>>str;
                    //getline(cin, str);
                    char stroka[100];
                    strcpy(stroka, str.c_str());
                    enterNum = false;
                    cout << str << endl;
                    for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
                    {
                        if (!isdigit(stroka[i]))
                        {
                            enterNum = true;
                            cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                            break;
                        }
                    }
                }
                metroLine line = opt_value.value();
                line.amountOfstation = stoi(str);
                this->storage_->updatemetroLine(line, ch);
            }
            else if (ch == '3')
            {
                puts("Введіть новий колір:");
                string str;
                cin>>str;
                //getline(cin, str);
                metroLine line = opt_value.value();
                line.colourOfLine = str;
                this->storage_->updatemetroLine(line, ch);
            }
            else if (ch == '4')
            {
                updateInformationAboutLine = false;
            }
        }
    }
    else
    {
        system("clear");
        cout << "Cтанції із заданим ID у базі не існує" << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        cin.ignore();
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
}

void Cui::metroLineDeleteMenu(int id)
{
    optional<metroLine> opt_value = this->storage_->getmetroLineById(id);
    if (opt_value)
    {
        this->storage_->removemetroLine(id);
        cout << "Лінію видалено" << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        cin.ignore();
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
    else
    {
        system("clear");
        cout << "Лінії із заданим ID у базі не існує" << endl;
        cout << "1.Вихід" << endl;
        char ch;
        cin >> ch;
        cin.ignore();
        while (ch != '1')
        {
            puts("Введено некоректний символ, спробуйте будь ласка ще раз:");
            cin >> ch;
        }
    }
}
void Cui::metroLineCreateMenu()
{
    system("clear");
    cout << "Введіть назву лінії:" << endl;
    string nameOfLine;
    getline(cin, nameOfLine);
    cout << "Введіть кількість станцій:" << endl;
    string amountOfStation;
    bool enterNum = true;
    while (enterNum)
    {
        getline(cin, amountOfStation);
        char stroka[100];
        strcpy(stroka, amountOfStation.c_str());
        enterNum = false;
        for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
        {
            if (!isdigit(stroka[i]))
            {
                enterNum = true;
                cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                break;
            }
        }
    }

    cout << "Введіть колір лінії:" << endl;
    string colourOfLine;
    getline(cin, colourOfLine);
    metroLine line;
    line.nameOfline = nameOfLine;
    line.amountOfstation = stoi(amountOfStation);
    line.colourOfLine = colourOfLine;
    this->storage_->insertmetroLine(line);
}

void Cui::metroStationCreateMenu()
{
    system("clear");
    cout << "Введіть назву станції:" << endl;
    string nameOfStation;
    getline(cin, nameOfStation);
    cout << "Введіть годину відкриття:" << endl;
    string hourOfOpening;
    bool enterNum = true;
    while (enterNum)
    {
        cin >> hourOfOpening;
        char stroka[100];
        strcpy(stroka, hourOfOpening.c_str());
        enterNum = false;
        for (int i = 0; i < static_cast<int>(strlen(stroka)); i++)
        {
            if (!isdigit(stroka[i]))
            {
                enterNum = true;
                cout << "Не правильно вказане числове значення, спробуйте будь ласка, ще раз." << endl;
                break;
            }
        }
    }
    cout << "Введіть назву лінії:" << endl;
    string nameOfLine;
    cin.ignore(); //very interesting
    getline(cin, nameOfLine);
    metroStation station;
    station.nameOfStation = nameOfStation;
    station.nameOfLine = nameOfLine;
    station.hourOfOpening = stoi(hourOfOpening);
    this->storage_->insertmetroStation(station);
}
