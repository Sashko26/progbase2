#pragma once
#include <math.h>
#include <iostream>
#include <string>

using namespace std;
class DynArray
{
private:
  void enlarge(int newCapacity);
  float *_array;
  int _capacity;
  int _length;

public:
  DynArray();
  ~DynArray();
  void add(float value);
  int size();
  float getValueByIndex(int index);
  void print();
  void zeroFractionAtBegin();
  float &operator[](int index);
  void fromStringToList(string &str, const int N);
};

