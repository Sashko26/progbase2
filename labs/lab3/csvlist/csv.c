#include "csv.h"
#include "list.h"
char *createOnHeap(const char *str)
{
    char *meme = malloc(sizeof(char) * (strlen(str) + 1));
    strcpy(meme, str);
    return meme;
}
typedef enum
{
    CsvReadState_Normal,
    CsvReadState_Escape,
} CsvReadState;

void Csv_addRow(List *table, List *row)
{
    if (table->size == table->capacity)
    {
        List_realloc(table);
    }
    table->items[table->size] = row;
    table->size = table->size + 1;
}
void Csv_addInt(List *row, int value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    char stackBuf[100];
    sprintf(stackBuf, "%i", value);
    row->items[row->size] = createOnHeap(stackBuf);
    row->size = row->size + 1;
}
void Csv_addString(List *row, const char *value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    row->items[row->size] = createOnHeap(value);
    row->size = row->size + 1;
}
void Csv_addFloat(List *row, float value)
{
    if (row->size == row->capacity)
    {
        List_realloc(row);
    }
    char stackBuf[100];
    sprintf(stackBuf, "%f", value);
    row->items[row->size] = createOnHeap(stackBuf);
    row->size = row->size + 1;
}
//заповнення таблиці зі строки з екрануванням
void Csv_fillTableFromStringModify(List *csvTable, const char *csvString)
{
    char buf[100];
    int bufX = 0;
    char *p = (char *)csvString;
    List *pRow = List_alloc();
    typedef enum
    {
        CsvReadState_Normal,
        CsvReadState_Escape,
    } CsvReadState;
    CsvReadState state = CsvReadState_Normal;
    while (1)
    {
        if (state == CsvReadState_Normal)
        {
            if (*p == '"')
            {
                state = CsvReadState_Escape;
            }
            else
            {
                if (*p == '\n' || *p == ',' || *p == '\0')
                {
                    if (*p == ',')
                    {
                        buf[bufX] = '\0';
                        Csv_addString(pRow, buf);
                        bufX = 0;
                    }
                    else if (*p == '\n')
                    {
                        buf[bufX] = '\0';
                        Csv_addString(pRow, buf);
                        Csv_addRow(csvTable, pRow);
                        pRow = NULL;
                        pRow = List_alloc();
                        bufX = 0;
                    }
                    else if (*p == '\0')
                    {
                        buf[bufX] = '\0';
                        Csv_addRow(csvTable, pRow);
                        pRow = NULL;
                        bufX = 0;
                        break;
                    }
                }
                else
                {
                    buf[bufX] = *p;
                    bufX += 1;
                }
            }
        }
        else // escaped
        {
            if (*p == '"')
            {
                if (*(p + 1) == '"')
                {
                    buf[bufX] = '\"';
                    bufX += 1;

                    p += 1;
                }
                else
                {

                    if (*(p + 1) == ',' || *(p + 1) == '\n' || *(p + 1) == '\0')
                    {
                        state = CsvReadState_Normal;
                    }
                    else
                    {
                        fprintf(stderr, "CSV format error: invalid char '%c' after escape\n", *(p + 1));
                        abort();
                    }
                }
            }
            else
            {
                buf[bufX] = *p;
                bufX += 1;
            }
        }
        if (*p == '\0')
        {
            break;
        }
        p += 1;
    }
    List_clear(csvTable->items[csvTable->size - 1]);
    List_free(csvTable->items[csvTable->size - 1]);
    csvTable->size = csvTable->size - 1;
}
void Csv_fillTableFromString(List *csvTable, const char *csvString)
{

    char buf[100];
    int bufX = 0;
    char *p = (char *)csvString;
    List *pRow = List_alloc();
    while (1)
    {
        if (*p == ',')
        {
            buf[bufX] = '\0';
            Csv_addString(pRow, buf);
            bufX = 0;
        }
        else if (*p == '\n')
        {
            buf[bufX] = '\0';
            Csv_addString(pRow, buf);
            Csv_addRow(csvTable, pRow);
            pRow = NULL;
            pRow = List_alloc();
            bufX = 0;
        }
        else if (*p == '\0')
        {
            buf[bufX] = '\0';
            Csv_addRow(csvTable, pRow);
            pRow = NULL;
            bufX = 0;
        }
        else
        {
            buf[bufX] = *p;
            bufX += 1;
        }

        if (*p == '\0')
        {
            break;
        }
        p += 1;
    }
    List_clear(csvTable->items[csvTable->size - 1]);
    List_free(csvTable->items[csvTable->size - 1]);
    csvTable->size = csvTable->size - 1;
}

char *Csv_fillStringFromTable(List *Table)
{
    char *string;
    string = Csv_fillStringFromList((Table)->items[0]);
    for (int i = 1; i < List_size(Table); i++)
    {
        char *buf = Csv_fillStringFromList((Table)->items[i]);
        string = strcat(string, buf);
        free(buf);
    }
    return string;
}
char *Csv_fillStringFromTableModify(List *Table)
{
    char *string;
    string = Csv_fillStringFromListModify((Table)->items[0]);
    for (int i = 1; i < List_size(Table); i++)
    {
        char *buf = Csv_fillStringFromListModify((Table)->items[i]);
        string = strcat(string, buf);
        free(buf);
    }
    printf("hello world\n");
    return string;
}
char *Csv_fillStringFromListModify(List *Row)
{
    char *stroka = malloc(sizeof(char) * 1000);
    stroka[0] = '\0';
    for (int i = 0; i < List_size(Row) - 1; i++)
    {
        char string[1000] = "\0";
        string[0] = '\"';
        string[1] = '\0';
        char stringForParse[1000] = "\0";
        strcat(stringForParse, List_get(Row, i));
        stringForParse[strlen(stringForParse)] = '\0';

        int indexString = 1;
        for (int i = 0; i < strlen(stringForParse); i++)
        {
            if (stringForParse[i] == '\"')
            {
                string[indexString] = '\"';
                indexString++;
                string[indexString] = '\"';
                indexString++;
            }
            else
            {
                string[indexString] = stringForParse[i];
                indexString++;
            }
        }
        string[indexString] = '\0';
        strcat(stroka, string);
        strcat(stroka,"\"");
        strcat(stroka,",");
    }
    char string[1000] = "\0";
    string[0] = '\"';
    string[1] = '\0';
    char stringForParse[1000] = "\0";
    strcat(stringForParse, List_get(Row, List_size(Row) - 1));
    stringForParse[strlen(stringForParse)] = '\0';
    int indexString = 1;
    for (int i = 0; i < strlen(stringForParse); i++)
    {
        if (stringForParse[i] == '\"')
        {
            string[indexString] = '\"';
            indexString++;
            string[indexString] = '\"';
            indexString++;
        }
        else
        {
            string[indexString] = stringForParse[i];
            indexString++;
        }
    }
    string[indexString] = '\0';
    strcat(stroka, string);
    strcat(stroka,"\"");
    strcat(stroka, "\n");
    strcat(stroka, "\0");
    return stroka;
}

char *Csv_fillStringFromList(List *Row)
{
    char *string;
    char *stroka = malloc(sizeof(char) * 1000);
    stroka[0] = '\0';
    for (int i = 0; i < List_size(Row) - 1; i++)
    {
        string = List_get(Row, i);
        strcat(stroka, string);
        strcat(stroka, ",");
    }
    string = List_get(Row, List_size(Row) - 1);
    strcat(stroka, string);
    strcat(stroka, "\n");
    strcat(stroka, "\0");
    return stroka;
}

