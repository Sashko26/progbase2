#pragma once
#include "csv_storage.h"
class Cui
{
  Storage *const storage_;

  //metroStation
  void metroStationsMainMenu(); //ok
  void metroStationMenu(int entity_id);
  void metroStationUpdateMenu(int id);
  void metroStationDeleteMenu(int id);
  void metroStationCreateMenu();
  //metorLine
  void metroLinesMainMenu();
  void metroLineMenu(int entity_id);
  void metroLineUpdateMenu(int id);
  void metroLineDeleteMenu(int id);
  void metroLineCreateMenu();

  //print
  void printStringsTableLittle(CsvTable &table);
  void printStringsLittle(CsvRow &row);
  void printStrings(CsvRow &row);

public:
  Cui(Storage *storage) : storage_(storage) {}
  void show();
};
