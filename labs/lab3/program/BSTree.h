#include "StrStrMap.h"
#include "BinTree.h"
#include <list.h>
#include <csv.h>
//Tree header 
struct __BSTree 
{
    BinTree * root;  // a pointer to the root tree node
};
typedef struct __BSTree BSTree;

BSTree* BSTree_alloc();
void BSTree_init(BSTree *self);
void   BSTree_insert   (BSTree * self,  StrStrMap* Map);  // add unique
bool BSTree_lookup(BSTree *self, int key);  // check for value with a key
StrStrMap *BSTree_search(BSTree *self, int key); // get the value for a key
 StrStrMap* BSTree_delete(BSTree * self, int key); // delete the value for a key


void deleteElementsFromBSTreeWithoptionN(List* listOfMap,BSTree *tree,int n);
void BSTree_free(BSTree *self);
BSTree *createBSTreeFromlistMap(List* listOfMap);


///print
static void printValueOnLevel(BinTree * node, char pos, int depth);
void print(BinTree * node, char pos, int depth);
void printTree(BinTree * root);

void clearTree(BinTree * node);
BinTree* BStree_getRoot(BSTree* tree);

