#include "BinTree.h"
BinTree *BinTree_alloc(StrStrMap *value)
{
    BinTree *self = malloc(sizeof(BinTree));
    self->value = value;
    self->key = station_OrderLiteraInAlphabet(value);
    self->left = NULL;
    self->right = NULL;

    return self;
}
 void BinTree_free(BinTree *self)
{
    BinTree_deinit(self);
    free(self);
    return ;
}
 void BinTree_deinit(BinTree *self)
{
    self->value = NULL;
    self->key = -1;
    self->right = NULL;
    self->left = NULL;
}