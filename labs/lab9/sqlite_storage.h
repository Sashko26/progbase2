#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#include "storage.h"
#include <QSqlDatabase>
#include "metroLine.h"
class SqliteStorage : public Storage
{
    QSqlDatabase _db;
    const string dir_name_;
public:
    SqliteStorage(const string &dir_name) : dir_name_(dir_name)
    {
        _db = QSqlDatabase::addDatabase("QSQLITE");
    }



   bool open() ;
   bool close() ;
  // metroStations
   vector<metroStation> getAllmetroStations(void) ;
   optional<metroStation> getmetroStationById(int metroStation_id) ;
   bool updatemetroStation(const metroStation &metroStation) ;
   bool removemetroStation(int metroStation_id) ;
   int insertmetroStation(const metroStation &metroStation) ;
  // metroLine
   vector<metroLine> getAllmetroLines(void) ;
   optional<metroLine> getmetroLineById(int metroLine_id) ;
   bool updatemetroLine(const metroLine &metroLine) ;
   bool removemetroLine(int metroLine_id) ;
   int insertmetroLine(const metroLine &metroLine) ;




    vector<metroStation> getAllUserMetroStations(int user_id) ;

// users
  optional<User> getUserAuth(QString & username, QString & password) ;
 // links
  vector<metroLine> getAllLinesOfstation(int station_id) ;
  bool insertLineinStation(int station_id, int line_id) ;
  bool removeLineinStation(int station_id, int line_id);
};

#endif // SQLITE_STORAGE_H
