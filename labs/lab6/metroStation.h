#pragma once
#include <vector>
#include <string>
#include "csv.h"


using std::vector;
using std::string;
typedef struct metroStation
{
    int id;
    string nameOfStation;
    int hourOfOpening;
    string nameOfLine;

} metroStation;
vector<metroStation> Csv_createArrayOfStructuresFromTable(CsvTable & csv_table);
void Csv_fromStructureArrayToTable(CsvTable & csv_table, vector<metroStation> &testList, int len);
vector<metroStation>Csv_createArrayOfStructures( CsvTable & csv_table);
void Csv_fromStructureArrayToTable(CsvTable & csv_table, vector<metroStation> &testList, int len);
void Csv_createTablefromArrayStructuresWithOptionN(CsvTable & csv_table, int len, vector<metroStation> &testList, int N);




