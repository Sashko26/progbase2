#include <unistd.h>
#include <getopt.h>

#include "csv.h"
#include "list.h"
#include "metroStation.h"
#include "Table.h"

void whenWeHaveInputFile(char *inputFile, List *Table, const char *state);
int main(int argc, char *argv[])
{
    metroStation *testList = createTestList();
    int lenTest = 8;
    int opt;
    bool optionO = false;
    char valueO[100];
    bool optionN = false;
    bool extraArg = false;
    char valueN[100] = "\0";
    char extraArgument[100] = "\0";

    while ((opt = getopt(argc, argv, "n:o:")) != -1)
    {

        switch (opt)
        {
        case 'n':
            printf("option: %c, value: %s\n", opt, optarg);
            optionN = true;
            strcpy(valueN, optarg);
            break;
        case 'o':
            printf("option: %c, value: %s\n", opt, optarg);
            optionO = true;
            strcpy(valueO, optarg);
            break;
        case ':':
            printf("option needs a value\n");
            break;
        case '?':
            printf("unknown option: %c\n", optopt);
            break;
        }
    }
    for (; optind < argc; optind++)
    {
        printf("extra arguments: %s\n", argv[optind]);
        extraArg = true;
        strcpy(extraArgument, argv[optind]);
    }
    if (extraArg == true && optionO == false && optionN == false)
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        printStringsTable(&Table);
        deinitTable(&Table);
        free(testList);
    }
    else if (extraArg == false && optionO == true && optionN == false)
    {
        List table;
        List_init(&table);
        printf("%s\n", valueO);
        FILE *fp;
        fp = fopen(valueO, "w");
        Csv_fromStructureArrayToTable(&table, testList, lenTest);
        char *csvString = Csv_fillStringFromTable(&table);
        puts(csvString);
        fputs(csvString, fp);
        free(csvString);
        deinitTable(&table);
        fclose(fp);
        free(testList);
    }
    //work
    else if (extraArg == false && optionO == false && optionN == true)
    {
        List table;
        List_init(&table);
        int N = atoi(valueN);
        Csv_createTablefromArrayStructuresWithOptionN(&table, lenTest, testList, N);
        printStringsTable(&table);
        deinitTable(&table);
        free(testList);
    }
    //працює
    else if (extraArg == false && optionO == false && optionN == false)
    {
        List table;
        List_init(&table);
        Csv_fromStructureArrayToTable(&table, testList, lenTest);
        printStringsTable(&table);
        deinitTable(&table);
        free(testList);
    }
    //work
    else if (extraArg == true && optionO == true && optionN == false)
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        printStringsTable(&Table);
        char *csv_string = Csv_fillStringFromTable(&Table);
        FILE *fp1;
        fp1 = fopen(valueO, "w");
        fputs(csv_string, fp1);
        free(csv_string);
        fclose(fp1);
        deinitTable(&Table);
        free(testList);
    }
    //work
    else if (extraArg == true && optionO == false && optionN == true)
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        metroStation *arrayOfstation = Csv_createArrayOfStructuresFromTable(&Table);
        int N = atoi(valueN);
        List table = createTable();
        int len = List_size(&Table);                                                       // v dopomogu ne zabut
        Csv_createTablefromArrayStructuresWithOptionN(&table, len - 1, arrayOfstation, N); //Другий випадок використання Len-1;
        printStringsTable(&table);
        deinitTable(&Table);
        deinitTable(&table);
        free(arrayOfstation);
        free(testList);
    }
    else if (extraArg == false && optionO == true && optionN == true)
    {
        List table = createTable();
        printf("%s\n", valueO);
        FILE *fp;
        fp = fopen(valueO, "w");
        Csv_createTablefromArrayStructuresWithOptionN(&table, lenTest, testList, atoi(valueN)); //цікаво, подивись на реалізацію
        char *csvString = Csv_fillStringFromTable(&table);
        puts(csvString);
        fputs(csvString, fp);
        free(csvString);
        deinitTable(&table);
        fclose(fp);
        free(testList);
    }
    else if (extraArg == true && optionO == true && optionN == true)
    {
        List Table = createTable();
        whenWeHaveInputFile(extraArgument, &Table, "r");
        metroStation *arrayOfStructures = Csv_createArrayOfStructuresFromTable(&Table);
        List table = createTable();
        Csv_createTablefromArrayStructuresWithOptionN(&table, List_size(&Table)-1, arrayOfStructures, atoi(valueN));
        printStringsTable(&table);
        char *Csv_string = Csv_fillStringFromTable(&table);
        FILE *fp1;
        fp1 = fopen(valueO, "w");
        fputs(Csv_string, fp1);
        fclose(fp1);
        free(Csv_string);
        deinitTable(&Table);
        deinitTable(&table);
        free(testList);
        free(arrayOfStructures);
    }

    return 0;
}

void whenWeHaveInputFile(char *inputFile, List *Table, const char *state)
{
    FILE *fp = NULL;
    fp = fopen(inputFile, state);
    if (fp == NULL)
    {
        abort();
    }
    int index = -1;
    const int LEN = 100000;
    char *str = malloc(sizeof(char) * LEN);
    do
    {
        index++;
        str[index] = fgetc(fp);
    } while (str[index] != EOF);
    str[index] = '\0';
    Csv_fillTableFromString(Table, str);
    fclose(fp);
    free(str);
}
