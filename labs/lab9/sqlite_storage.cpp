#include "sqlite_storage.h"
#include <QtSql>
#include <QString>
#include <iostream>
#include <metroLine.h>
#include <metroStation.h>
#include "user.h"
#include <QVariant>

QString hashPassword(QString const & pass)
{
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}



bool SqliteStorage::open()
{
    string path=this->dir_name_+"/data.sql";

    QString q_path=QString::fromStdString(path);
    _db.setDatabaseName(q_path);    // set sqlite database file path
    bool connected = _db.open();  // open db connection
    if (!connected)
    {
        puts(path.c_str());
        puts("ne vidkrylo!");
        return false;
    }
      return true;
}
bool SqliteStorage::close()
{
    _db.close();
    return true;
}
// metroStations

vector<metroStation> SqliteStorage::getAllmetroStations(void)
{
    vector<metroStation> metroStations;
    QSqlQuery query("SELECT * FROM metro_stations");

    while (query.next())
    {
       metroStation station;
       QString station_name = query.value("station_name").toString();
       int id = query.value("id").toInt();
       int opening_time = query.value("opening_time").toInt();
       QString line_name = query.value("line_name").toString();
      // qDebug() << id << " | " << station_name; qdebug виводить
      //повідомлення в консоль, тому не дуже гарно, коли
      //програма консольна і виводяться технічні повідомлення
       station.id=id;
       station.nameOfStation=station_name.toStdString();
       station.nameOfLine=line_name.toStdString();
       station.hourOfOpening=opening_time;
       metroStations.push_back(station);
    }
    return metroStations;
}
optional<metroStation> SqliteStorage::getmetroStationById(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_station error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroStation station;
              QString station_name = query.value("station_name").toString();
              int id = query.value("id").toInt();
              int opening_time = query.value("opening_time").toInt();
              QString line_name = query.value("line_name").toString();
              qDebug() << id << " | " << station_name;
              station.id=id;
              station.nameOfStation=station_name.toStdString();
              station.nameOfLine=line_name.toStdString();
              station.hourOfOpening=opening_time;
            return station;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroStation(const metroStation &metroStation)
{

    QSqlQuery query;
    query.prepare("UPDATE metro_stations SET station_name = :station_name , opening_time = :opening_time, line_name = :line_name WHERE id = :id");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    cout<<metroStation.hourOfOpening<<endl;
    query.bindValue(":opening_time", metroStation.hourOfOpening);
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":id", metroStation.id);


     if (!query.exec())
    {
        qDebug() << "update station error:" << query.lastError();
        cout<<metroStation.hourOfOpening<<endl;
        return false;
    }
    if(query.numRowsAffected()==0)
    {
        return false;
        puts("hi mazafucker");
        abort();
    }




    return true;
}
bool SqliteStorage::removemetroStation(int metroStation_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_stations WHERE id = :id");
    query.bindValue(":id", metroStation_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_station error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroStation(const metroStation &metroStation)
{

    QSqlQuery query;
    query.prepare("INSERT INTO metro_stations (station_name, opening_time, line_name) VALUES (:station_name, :opening_time, :line_name)");
    query.bindValue(":station_name", QString::fromStdString(metroStation.nameOfStation));
    query.bindValue(":line_name", QString::fromStdString(metroStation.nameOfLine));
    query.bindValue(":opening_time", metroStation.hourOfOpening);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();
}




// metroLine
vector<metroLine> SqliteStorage::getAllmetroLines(void)
{
    vector<metroLine> metroLines;
    QSqlQuery query("SELECT * FROM metro_lines");

    while (query.next())
    {
       metroLine line;
       QString line_name = query.value("line_name").toString();
       int id = query.value("id").toInt();
       int amount_stations = query.value("amount_stations").toInt();
       QString color_line= query.value("color_line").toString();
      // qDebug() << id << " | " << station_name; qdebug виводить
      //повідомлення в консоль, тому не дуже гарно, коли
      //програма консольна і виводяться технічні повідомлення
       line.id=id;
       line.nameOfline=line_name.toStdString();
       line.colourOfLine=color_line.toStdString();
       line.amountOfstation=amount_stations;
       metroLines.push_back(line);
    }
    return metroLines;

}
optional<metroLine> SqliteStorage::getmetroLineById(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        // do exec if query is prepared SELECT query
       qDebug() << "get metro_station error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
                qDebug() << " found ";
              metroLine line;
              QString line_name = query.value("line_name").toString();
              int id = query.value("id").toInt();
              int amount_stations = query.value("amount_stations").toInt();
              QString color_line = query.value("color_line").toString();
              //qDebug() << id << " | " << line_name;
              line.id=id;
              line.nameOfline=line_name.toStdString();
              line.colourOfLine=color_line.toStdString();
              line.amountOfstation=amount_stations;
            return line;
    }
    else
    {
       qDebug() << " not found ";
       return nullopt;
    }

    return nullopt;
}
bool SqliteStorage::updatemetroLine(const metroLine &metroLine)
{
    QSqlQuery query;
    query.prepare("UPDATE metro_lines SET line_name = :line_name, amount_stations = :amount_stations, color_line = :color_line WHERE id = :id");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":id", metroLine.id);
 if (!query.exec())
    {
        qDebug() << "update Line error:" << query.lastError();
        return false;

    }
    if(query.numRowsAffected()==0)
    {
        return false;
    }


    return true;
}
bool SqliteStorage::removemetroLine(int metroLine_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM metro_lines WHERE id = :id");
    query.bindValue(":id", metroLine_id);
    if (!query.exec())
    {
        qDebug() << "delete metro_line error:" << query.lastError();
          return false;
    }
    if(query.numRowsAffected()==0)
    {
        qDebug()<< query.numRowsAffected();
        return false;
    }
    return true;
}
int SqliteStorage::insertmetroLine(const metroLine &metroLine)
{
    QSqlQuery query;
    query.prepare("INSERT INTO metro_lines (line_name, amount_stations, color_line) VALUES (:line_name, :amount_stations, :color_line)");
    query.bindValue(":line_name", QString::fromStdString(metroLine.nameOfline));
    query.bindValue(":color_line", QString::fromStdString(metroLine.colourOfLine));
    query.bindValue(":amount_stations", metroLine.amountOfstation);
    if (!query.exec())
    {
        qDebug() << "add metro_station error:" << query.lastError();
    }
    QVariant var= query.lastInsertId();

    return var.toInt();
    return 0;
}

 vector<metroStation> SqliteStorage::getAllUserMetroStations(int user_id)
 {
    QSqlQuery query;
    query.prepare("SELECT * FROM metro_stations "
                  "WHERE user_id = :user_id;");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    vector<metroStation> metroStations;
    while (query.next())
    {
        metroStation station;
        station.id = query.value("id").toInt();
        station.nameOfStation = query.value("station_name").toString().toStdString();
        station.nameOfLine = query.value("line_name").toString().toStdString();
        station.hourOfOpening=query.value("opening_time").toInt();
        metroStations.push_back(station);
    }
    return metroStations;
}

// users
 optional<User> SqliteStorage::getUserAuth(QString & username, QString & password)
 {
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username;");
    query.bindValue(":username", username);
//    query.bindValue(":password_hash", hashPassword(password));
    if (!query.exec())
    {
        QSqlError error = query.lastError();
        throw error;
    }
    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = query.value("username").toString();
        user.passoword_hash = query.value("passoword_hash").toString();
        optional<User> opt_value;
        opt_value=user;
        return opt_value;
    }
    return nullopt;
 }

// links



 //need cheking
 vector<metroLine> SqliteStorage::getAllLinesOfstation(int station_id)
 {

     QSqlQuery query;
     query.prepare("SELECT * FROM links WHERE main_entity_id = :station_id;");
     query.bindValue(":station_id", station_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     vector<int> id_of_entity;
     while (query.next())
     {
         int num;
         num=query.value("extra_entity_id").toInt();
          id_of_entity.push_back(num);


     }
     vector<metroLine> metroLines;
     for(int i=0;i<static_cast<int>(id_of_entity.size());i++)
     {
        int num =id_of_entity.at(i);
        query.prepare("SELECT * FROM metro_lines "
                      " id = :num;");
        query.bindValue(":num", num);
        if (!query.exec())
        {
            QSqlError error = query.lastError();
            throw error;
        }

        while (query.next())
        {
                metroLine line;
               line.id=query.value("id").toInt();
               line.nameOfline=query.value("line_name").toString().toStdString();
               line.colourOfLine=query.value("color_line").toString().toStdString();
               line.amountOfstation=query.value("amount_stations").toInt();
               metroLines.push_back(line);
         }

     }

                return metroLines;
 }


 bool SqliteStorage::insertLineinStation(int station_id, int line_id)
 {
     QSqlQuery query;
     query.prepare("INSERT INTO links (main_entity_id, extra_entity_id) VALUES (:station_id, line_id)");
     query.bindValue(":station_id",station_id);
     query.bindValue(":line_id",line_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     return query.lastInsertId().toBool();





 }
 bool SqliteStorage::removeLineinStation(int station_id, int line_id)
 {
     QSqlQuery query;
     query.prepare("DELETE FROM links  main_entity_id = :statoin_id AND extra_entity_id = :line_id");
     query.bindValue(":station_id",station_id);
     query.bindValue(":line_id",line_id);
     if (!query.exec())
     {
         QSqlError error = query.lastError();
         throw error;
     }
     if(query.numRowsAffected()==0)
     {
         qDebug()<< query.numRowsAffected();
         return false;
     }
        return true;
 }
