#include "editdialog.h"
#include "ui_editdialog.h"


EditDialog::EditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditDialog)
{
    ui->setupUi(this);
}

EditDialog::~EditDialog()
{
    delete ui;
}
void EditDialog::fillingLabebsInEditDialogDefaultData(metroStation st)
{
    QString nameOfStation =QString::fromStdString(st.nameOfStation);
    QString nameOfLine=QString::fromStdString(st.nameOfLine);
    int openingTime=st.hourOfOpening;
    this->ui->lineEdit_stName->setText(nameOfStation);


    //this->ui->lineEdit_openTime->setText(openingTime);
    this->ui->spinBox->setValue(openingTime);


    this->ui->lineEdit_lineName->setText(nameOfLine);
}
bool EditDialog::checkLinesInEditDialog()
{
    QString stationName=this->ui->lineEdit_stName->text();


    //QString openingTime=this->ui->lineEdit_openTime->text();


    QString lineName=this->ui->lineEdit_lineName->text();
    if(stationName.size()==0 || lineName.size()==0)
    {
        return false;
    }
    return true;
}
metroStation EditDialog::getInstance()
{
    metroStation station;
    string st_name = this->ui->lineEdit_stName->text().toStdString();
    string line_name = this->ui->lineEdit_lineName->text().toStdString();


    //int open_time=this->ui->lineEdit_openTime->text().toInt();
    int open_time=this->ui->spinBox->value();

    station.nameOfStation=st_name;
    station.hourOfOpening=open_time;
    station.nameOfLine=line_name;
    return station;
}
