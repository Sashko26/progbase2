#include "authentication.h"
#include "ui_authentication.h"
#include "sqlite_storage.h"
#include "user.h"

authentication::authentication(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::authentication)
{
    ui->setupUi(this);
}

authentication::~authentication()
{
    delete ui;
}
QString authentication::getPassword()
{
   return ui->lineEdit_password->text();
}
QString authentication::getLogin()
{
   return ui->lineEdit_login->text();
}
void authentication::LabelInCorrect()
{
    this->ui->label_invorrect->setText("введений неправильний логін або пароль."
                                       "Спробуйте будь ласка ще раз");

}
