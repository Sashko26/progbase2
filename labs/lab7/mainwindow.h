#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "storage.h"
#include <QListWidget>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void setData(metroStation metrostation);
    void clear();
    ~MainWindow();

private slots:
    void New_Storage();
    void Open_Storage();
    void beforeClose();


    void on_listWidget_itemClicked(QListWidgetItem *item);

    void on_addButton_clicked();

    void on_removeButton_clicked();

    void on_editButton_clicked();

private:
    Ui::MainWindow *ui;
    Storage * storage_; // <-- STORAGE
};

#endif // MAINWINDOW_H
