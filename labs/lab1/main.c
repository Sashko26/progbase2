// Компілювати за допомогою:
// gcc main.c -lprogbase -lm

#include <stdio.h>  // Для друку в термінал
#include <stdlib.h> // Деякі додаткові функції
#include <string.h>

#include "list.h"
#include "deque.h"

int main()
{
    FILE *fp = fopen("data.txt", "r");
    if (fp == NULL)
    {
        return EXIT_FAILURE;
    }
    int index = -1;
    const int LEN = 1000;
    char str[LEN];
    do
    {
        index++;
        str[index] = fgetc(fp);
    } while (str[index] != EOF);
    str[index] = '\0';
    /* if (fp == NULL)
    {
        fprintf(stderr, "Не вдалось відкрити файл!");
    } */
    const int N = 1000;
    //ініціалізація динамічного масиву
    DynArray list;
    DynArray_init(&list);
    /* char str[N]; */
    fgets(str, N, fp);
    printf("%s", str);
    printf("|%ld|\n", strlen(str));
    fclose(fp);
    DynArray_fromStringToArray(&list, str, N);
    puts("cписок, який складається з чисел текстового файлу:");
    DynArray_print(&list);
    DynArray_zeroFractionAtBegin(&list);
    puts("відсортований cписок, у якому елементи з нульовим дробом на початку:");
    DynArray_print(&list);
    Deque firstDeque;
    Deque_init(&firstDeque);
    Deque secondDeque;
    Deque_init(&secondDeque);
    for (int i = 0; i < DynArray_size(&list); i++)
    {
        if (i % 2 == 0)
        {
            Deque_pushByFront(&firstDeque, DynArray_getValueByIndex(&list, i));
        }
        else
        {
            Deque_pushByRear(&secondDeque, DynArray_getValueByIndex(&list, i));
        }
    }
    puts("перша дека, у якій всі елементи парних позицій елементи відсортованого списку були додані на початок:");
    Deque_print(&firstDeque);
    puts("друга дека, у якій всі елементи непарних позицій відсортованого списку були додані в кінець:");
    Deque_print(&secondDeque);
    DynArray newList;
    DynArray_init(&newList);

    for (int i = Deque_size(&firstDeque) - 1; i >= 0; i--)
    {
        DynArray_add(&newList, Deque_popByFront(&firstDeque));
    }
    printf("%i\n",Deque_size(&secondDeque));

    for (int i = Deque_size(&secondDeque) - 1; i >= 0; i--)
    {
        DynArray_add(&newList, Deque_popByRear(&secondDeque));
    }
    puts("новий список, у якому переписані всі елементи з початку першої деки і з кінця другої:");
    DynArray_print(&newList);
    printf( "%i\n",DynArray_size(&newList));

    // звільнення динамічної пам'яті
    DynArray_deinit(&newList);
    Deque_deinit(&firstDeque);
    Deque_deinit(&secondDeque);
    DynArray_deinit(&list);

    return 0;
}
